(self["webpackChunktergo_calculator"] = self["webpackChunktergo_calculator"] || []).push([["createFlight"],{

/***/ "./app/airportsCountries.js":
/*!**********************************!*\
  !*** ./app/airportsCountries.js ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ([
    {
        "id": 1,
        "text": "Papua New Guinea"
    },
    {
        "id": 2,
        "text": "Greenland"
    },
    {
        "id": 3,
        "text": "Iceland"
    },
    {
        "id": 4,
        "text": "Canada"
    },
    {
        "id": 5,
        "text": "Algeria"
    },
    {
        "id": 6,
        "text": "Benin"
    },
    {
        "id": 7,
        "text": "Burkina Faso"
    },
    {
        "id": 8,
        "text": "Ghana"
    },
    {
        "id": 9,
        "text": "Cote d'Ivoire"
    },
    {
        "id": 10,
        "text": "Nigeria"
    },
    {
        "id": 11,
        "text": "Niger"
    },
    {
        "id": 12,
        "text": "Tunisia"
    },
    {
        "id": 13,
        "text": "Togo"
    },
    {
        "id": 14,
        "text": "Belgium"
    },
    {
        "id": 15,
        "text": "Germany"
    },
    {
        "id": 16,
        "text": "Estonia"
    },
    {
        "id": 17,
        "text": "Finland"
    },
    {
        "id": 18,
        "text": "United Kingdom"
    },
    {
        "id": 19,
        "text": "Guernsey"
    },
    {
        "id": 20,
        "text": "Jersey"
    },
    {
        "id": 21,
        "text": "Isle of Man"
    },
    {
        "id": 22,
        "text": "Falkland Islands"
    },
    {
        "id": 23,
        "text": "Netherlands"
    },
    {
        "id": 24,
        "text": "Ireland"
    },
    {
        "id": 25,
        "text": "Denmark"
    },
    {
        "id": 26,
        "text": "Faroe Islands"
    },
    {
        "id": 27,
        "text": "Luxembourg"
    },
    {
        "id": 28,
        "text": "Norway"
    },
    {
        "id": 29,
        "text": "Poland"
    },
    {
        "id": 30,
        "text": "Sweden"
    },
    {
        "id": 31,
        "text": "South Africa"
    },
    {
        "id": 32,
        "text": "Botswana"
    },
    {
        "id": 33,
        "text": "Congo (Brazzaville)"
    },
    {
        "id": 34,
        "text": "Congo (Kinshasa)"
    },
    {
        "id": 35,
        "text": "Swaziland"
    },
    {
        "id": 36,
        "text": "Central African Republic"
    },
    {
        "id": 37,
        "text": "Equatorial Guinea"
    },
    {
        "id": 38,
        "text": "Saint Helena"
    },
    {
        "id": 39,
        "text": "Mauritius"
    },
    {
        "id": 40,
        "text": "British Indian Ocean Territory"
    },
    {
        "id": 41,
        "text": "Cameroon"
    },
    {
        "id": 42,
        "text": "Zambia"
    },
    {
        "id": 43,
        "text": "Comoros"
    },
    {
        "id": 44,
        "text": "Mayotte"
    },
    {
        "id": 45,
        "text": "Reunion"
    },
    {
        "id": 46,
        "text": "Madagascar"
    },
    {
        "id": 47,
        "text": "Angola"
    },
    {
        "id": 48,
        "text": "Gabon"
    },
    {
        "id": 49,
        "text": "Sao Tome and Principe"
    },
    {
        "id": 50,
        "text": "Mozambique"
    },
    {
        "id": 51,
        "text": "Seychelles"
    },
    {
        "id": 52,
        "text": "Chad"
    },
    {
        "id": 53,
        "text": "Zimbabwe"
    },
    {
        "id": 54,
        "text": "Malawi"
    },
    {
        "id": 55,
        "text": "Lesotho"
    },
    {
        "id": 56,
        "text": "Mali"
    },
    {
        "id": 57,
        "text": "Gambia"
    },
    {
        "id": 58,
        "text": "Spain"
    },
    {
        "id": 59,
        "text": "Sierra Leone"
    },
    {
        "id": 60,
        "text": "Guinea-Bissau"
    },
    {
        "id": 61,
        "text": "Liberia"
    },
    {
        "id": 62,
        "text": "Morocco"
    },
    {
        "id": 63,
        "text": "Senegal"
    },
    {
        "id": 64,
        "text": "Mauritania"
    },
    {
        "id": 65,
        "text": "Guinea"
    },
    {
        "id": 66,
        "text": "Cape Verde"
    },
    {
        "id": 67,
        "text": "Ethiopia"
    },
    {
        "id": 68,
        "text": "Burundi"
    },
    {
        "id": 69,
        "text": "Somalia"
    },
    {
        "id": 70,
        "text": "Egypt"
    },
    {
        "id": 71,
        "text": "Kenya"
    },
    {
        "id": 72,
        "text": "Libya"
    },
    {
        "id": 73,
        "text": "Rwanda"
    },
    {
        "id": 74,
        "text": "Sudan"
    },
    {
        "id": 75,
        "text": "South Sudan"
    },
    {
        "id": 76,
        "text": "Tanzania"
    },
    {
        "id": 77,
        "text": "Uganda"
    },
    {
        "id": 78,
        "text": "Albania"
    },
    {
        "id": 79,
        "text": "Bulgaria"
    },
    {
        "id": 80,
        "text": "Cyprus"
    },
    {
        "id": 81,
        "text": "Croatia"
    },
    {
        "id": 82,
        "text": "France"
    },
    {
        "id": 83,
        "text": "Saint Pierre and Miquelon"
    },
    {
        "id": 84,
        "text": "Greece"
    },
    {
        "id": 85,
        "text": "Hungary"
    },
    {
        "id": 86,
        "text": "Italy"
    },
    {
        "id": 87,
        "text": "Slovenia"
    },
    {
        "id": 88,
        "text": "Czech Republic"
    },
    {
        "id": 89,
        "text": "Israel"
    },
    {
        "id": 90,
        "text": "Malta"
    },
    {
        "id": 91,
        "text": "Austria"
    },
    {
        "id": 92,
        "text": "Portugal"
    },
    {
        "id": 93,
        "text": "Bosnia and Herzegovina"
    },
    {
        "id": 94,
        "text": "Romania"
    },
    {
        "id": 95,
        "text": "Switzerland"
    },
    {
        "id": 96,
        "text": "Turkey"
    },
    {
        "id": 97,
        "text": "Moldova"
    },
    {
        "id": 98,
        "text": "Macedonia"
    },
    {
        "id": 99,
        "text": "Gibraltar"
    },
    {
        "id": 100,
        "text": "Serbia"
    },
    {
        "id": 101,
        "text": "Montenegro"
    },
    {
        "id": 102,
        "text": "Slovakia"
    },
    {
        "id": 103,
        "text": "Turks and Caicos Islands"
    },
    {
        "id": 104,
        "text": "Dominican Republic"
    },
    {
        "id": 105,
        "text": "Guatemala"
    },
    {
        "id": 106,
        "text": "Honduras"
    },
    {
        "id": 107,
        "text": "Jamaica"
    },
    {
        "id": 108,
        "text": "Mexico"
    },
    {
        "id": 109,
        "text": "Nicaragua"
    },
    {
        "id": 110,
        "text": "Panama"
    },
    {
        "id": 111,
        "text": "Costa Rica"
    },
    {
        "id": 112,
        "text": "El Salvador"
    },
    {
        "id": 113,
        "text": "Haiti"
    },
    {
        "id": 114,
        "text": "Cuba"
    },
    {
        "id": 115,
        "text": "Cayman Islands"
    },
    {
        "id": 116,
        "text": "Bahamas"
    },
    {
        "id": 117,
        "text": "Belize"
    },
    {
        "id": 118,
        "text": "Cook Islands"
    },
    {
        "id": 119,
        "text": "Fiji"
    },
    {
        "id": 120,
        "text": "Tonga"
    },
    {
        "id": 121,
        "text": "Kiribati"
    },
    {
        "id": 122,
        "text": "Wallis and Futuna"
    },
    {
        "id": 123,
        "text": "Samoa"
    },
    {
        "id": 124,
        "text": "American Samoa"
    },
    {
        "id": 125,
        "text": "French Polynesia"
    },
    {
        "id": 126,
        "text": "Vanuatu"
    },
    {
        "id": 127,
        "text": "New Caledonia"
    },
    {
        "id": 128,
        "text": "New Zealand"
    },
    {
        "id": 129,
        "text": "Antarctica"
    },
    {
        "id": 130,
        "text": "Afghanistan"
    },
    {
        "id": 131,
        "text": "Bahrain"
    },
    {
        "id": 132,
        "text": "Saudi Arabia"
    },
    {
        "id": 133,
        "text": "Iran"
    },
    {
        "id": 134,
        "text": "Jordan"
    },
    {
        "id": 135,
        "text": "West Bank"
    },
    {
        "id": 136,
        "text": "Kuwait"
    },
    {
        "id": 137,
        "text": "Lebanon"
    },
    {
        "id": 138,
        "text": "United Arab Emirates"
    },
    {
        "id": 139,
        "text": "Oman"
    },
    {
        "id": 140,
        "text": "Pakistan"
    },
    {
        "id": 141,
        "text": "Iraq"
    },
    {
        "id": 142,
        "text": "Syria"
    },
    {
        "id": 143,
        "text": "Qatar"
    },
    {
        "id": 144,
        "text": "Northern Mariana Islands"
    },
    {
        "id": 145,
        "text": "Guam"
    },
    {
        "id": 146,
        "text": "Marshall Islands"
    },
    {
        "id": 147,
        "text": "Midway Islands"
    },
    {
        "id": 148,
        "text": "Micronesia"
    },
    {
        "id": 149,
        "text": "Palau"
    },
    {
        "id": 150,
        "text": "Taiwan"
    },
    {
        "id": 151,
        "text": "Japan"
    },
    {
        "id": 152,
        "text": "South Korea"
    },
    {
        "id": 153,
        "text": "Philippines"
    },
    {
        "id": 154,
        "text": "Argentina"
    },
    {
        "id": 155,
        "text": "Brazil"
    },
    {
        "id": 156,
        "text": "Chile"
    },
    {
        "id": 157,
        "text": "Ecuador"
    },
    {
        "id": 158,
        "text": "Paraguay"
    },
    {
        "id": 159,
        "text": "Colombia"
    },
    {
        "id": 160,
        "text": "Bolivia"
    },
    {
        "id": 161,
        "text": "Suriname"
    },
    {
        "id": 162,
        "text": "French Guiana"
    },
    {
        "id": 163,
        "text": "Peru"
    },
    {
        "id": 164,
        "text": "Uruguay"
    },
    {
        "id": 165,
        "text": "Venezuela"
    },
    {
        "id": 166,
        "text": "Guyana"
    },
    {
        "id": 167,
        "text": "Antigua and Barbuda"
    },
    {
        "id": 168,
        "text": "Barbados"
    },
    {
        "id": 169,
        "text": "Dominica"
    },
    {
        "id": 170,
        "text": "Martinique"
    },
    {
        "id": 171,
        "text": "Guadeloupe"
    },
    {
        "id": 172,
        "text": "Grenada"
    },
    {
        "id": 173,
        "text": "Virgin Islands"
    },
    {
        "id": 174,
        "text": "Puerto Rico"
    },
    {
        "id": 175,
        "text": "Saint Kitts and Nevis"
    },
    {
        "id": 176,
        "text": "Saint Lucia"
    },
    {
        "id": 177,
        "text": "Aruba"
    },
    {
        "id": 178,
        "text": "Netherlands Antilles"
    },
    {
        "id": 179,
        "text": "Anguilla"
    },
    {
        "id": 180,
        "text": "Trinidad and Tobago"
    },
    {
        "id": 181,
        "text": "British Virgin Islands"
    },
    {
        "id": 182,
        "text": "Saint Vincent and the Grenadines"
    },
    {
        "id": 183,
        "text": "Kazakhstan"
    },
    {
        "id": 184,
        "text": "Kyrgyzstan"
    },
    {
        "id": 185,
        "text": "Azerbaijan"
    },
    {
        "id": 186,
        "text": "Russia"
    },
    {
        "id": 187,
        "text": "Ukraine"
    },
    {
        "id": 188,
        "text": "Belarus"
    },
    {
        "id": 189,
        "text": "Turkmenistan"
    },
    {
        "id": 190,
        "text": "Tajikistan"
    },
    {
        "id": 191,
        "text": "Uzbekistan"
    },
    {
        "id": 192,
        "text": "India"
    },
    {
        "id": 193,
        "text": "Sri Lanka"
    },
    {
        "id": 194,
        "text": "Cambodia"
    },
    {
        "id": 195,
        "text": "Bangladesh"
    },
    {
        "id": 196,
        "text": "Hong Kong"
    },
    {
        "id": 197,
        "text": "Laos"
    },
    {
        "id": 198,
        "text": "Macau"
    },
    {
        "id": 199,
        "text": "Nepal"
    },
    {
        "id": 200,
        "text": "Bhutan"
    },
    {
        "id": 201,
        "text": "Maldives"
    },
    {
        "id": 202,
        "text": "Thailand"
    },
    {
        "id": 203,
        "text": "Vietnam"
    },
    {
        "id": 204,
        "text": "Burma"
    },
    {
        "id": 205,
        "text": "Indonesia"
    },
    {
        "id": 206,
        "text": "Malaysia"
    },
    {
        "id": 207,
        "text": "Brunei"
    },
    {
        "id": 208,
        "text": "East Timor"
    },
    {
        "id": 209,
        "text": "Singapore"
    },
    {
        "id": 210,
        "text": "Australia"
    },
    {
        "id": 211,
        "text": "Christmas Island"
    },
    {
        "id": 212,
        "text": "Norfolk Island"
    },
    {
        "id": 213,
        "text": "China"
    },
    {
        "id": 214,
        "text": "North Korea"
    },
    {
        "id": 215,
        "text": "Mongolia"
    },
    {
        "id": 216,
        "text": "United States"
    },
    {
        "id": 217,
        "text": "Latvia"
    },
    {
        "id": 218,
        "text": "Lithuania"
    },
    {
        "id": 219,
        "text": "Armenia"
    },
    {
        "id": 220,
        "text": "Eritrea"
    },
    {
        "id": 221,
        "text": "Palestine"
    },
    {
        "id": 222,
        "text": "Georgia"
    },
    {
        "id": 223,
        "text": "Yemen"
    },
    {
        "id": 224,
        "text": "Bermuda"
    },
    {
        "id": 225,
        "text": "Solomon Islands"
    },
    {
        "id": 226,
        "text": "Nauru"
    },
    {
        "id": 227,
        "text": "Tuvalu"
    },
    {
        "id": 228,
        "text": "Namibia"
    },
    {
        "id": 229,
        "text": "Djibouti"
    },
    {
        "id": 230,
        "text": "Montserrat"
    },
    {
        "id": 231,
        "text": "Johnston Atoll"
    },
    {
        "id": 232,
        "text": "Western Sahara"
    },
    {
        "id": 233,
        "text": "Niue"
    },
    {
        "id": 234,
        "text": "Cocos (Keeling) Islands"
    },
    {
        "id": 235,
        "text": "Myanmar"
    },
    {
        "id": 236,
        "text": "Svalbard"
    },
    {
        "id": 237,
        "text": "Wake Island"
    },
    {
        "id": 238,
        "text": "Illertissen see EDMI - ED-0425 location moved out-of-the way"
    }
]);

/***/ }),

/***/ "./app/createFlight.js":
/*!*****************************!*\
  !*** ./app/createFlight.js ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _airportsCountries__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./airportsCountries */ "./app/airportsCountries.js");
/* harmony import */ var _airports__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./airports */ "./app/airports.js");



const isMobile = window.isMobile && window.isMobile.any;
const isENG = document.body.classList.contains('eng');

const createFlight = (id, parent) => {
  const li = document.createElement('li');
  li.setAttribute('data-flight-id', id);
  li.innerHTML = `<a class="uk-accordion-title" href="#"><span>${isENG ? 'Flight' : 'Lot'}</span><img src="img/remove-icon.png" class="remove"/></a>
  <div class="uk-accordion-content">
    <div class="uk-flex uk-form-controls">
    <label class="uk-form-label uk-margin-bottom uk-margin-right"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-direction-${id}" data-type="item" value="one-way">${isENG ? 'one-way trip' : 'podróż w jedną stronę'}</label>
    <label class="uk-form-label uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-direction-${id}" data-type="item" value="two-way">${isENG ? 'two-way trip' : 'podróż w dwie strony'}</label>
    </div>
    <div class="uk-flex uk-form-controls">
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom uk-margin-remove-left@m"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="economic" data-label="${isENG ? 'economic' : 'klasa ekonomiczna'}">${isENG ? 'economic' : 'klasa ekonomiczna'}</label>
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="business" data-label="${isENG ? 'business' : 'klasa biznes'}">${isENG ? 'business' : 'klasa biznes'}</label>
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="first-class" data-label="${isENG ? 'first class' : 'pierwsza klasa'}">${isENG ? 'first class' : 'pierwsza klasa'}</label>
    </div>
    <div class="uk-flex uk-form-controls uk-margin-top">
    <label style="width: 15%">${isENG ? 'From' : 'Z'}:</label>
    <div class="uk-flex uk-flex-column uk-margin-bottom" style="width: 85%">
      <select class="select2 airport-country-${id} from" style="width: 100%" type="select" data-type="airport-country" name="trav3">
      <option value="">${isENG ? 'Country' : 'Kraj'}</option>
      </select>
      <select class="select2 airport-name-${id} from" style="width: 100%" type="select" data-type="airport-name-from" name="trav3">
      <option value="">${isENG ? 'Airport' : 'Lotnisko'}</option>
      </select>
    </div>
    </div>
    <div class="uk-flex uk-form-controls">
    <label style="width: 15%">${isENG ? 'To' : 'Do'}:</label>
    <div class="uk-flex uk-flex-column" style="width: 85%">
      <select class="select2 airport-country-${id} to" style="width: 100%" type="select" data-type="airport-country" name="trav3">
      <option value="">${isENG ? 'Country' : 'Kraj'}</option>
      </select>
      <select class="select2 airport-name-${id} to" style="width: 100%" type="select" data-type="airport-name-to" name="trav3">
      <option value="">${isENG ? 'Airport' : 'Lotnisko'}</option>
      </select>
    </div>
    </div>
  </div>`;

  parent.appendChild(li);

  li.querySelector('.remove').addEventListener('click', e => {
    e.preventDefault();

    li.parentNode.removeChild(li);

    const planes = document.querySelector("[data-slide='trav3'] > .uk-flex > div:first-of-type");

    planes.removeChild(planes.lastElementChild);
  });

  let airportTo;
  let airportFrom;

  const countryFromSelect = $(`.airport-country-${id}.from`).select2({
    data: _airportsCountries__WEBPACK_IMPORTED_MODULE_0__.default,
    placeholder: isENG ? 'Country' : 'Kraj',
  });
  const airportFromSelect = $(`.airport-name-${id}.from`).select2({
    data: [],
    placeholder: isENG ? 'Airport' : 'Lotnisko',
  });

  airportFromSelect.on('select2:select', e => {
    const data = e.params.data;

    airportFrom = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = airportFrom.substr(0, isMobile ? 12 : 15) + '...' + (airportTo ? ' &times; ' + airportTo.substr(0, isMobile ? 12 : 15) + '...' : '');
  });

  countryFromSelect.on('select2:select', e => {
    const data = e.params.data;

    const airportsForCountry = _airports__WEBPACK_IMPORTED_MODULE_1__.default.filter(airports => airports.country === data.text);
    airportsForCountry.unshift({
      id: -1,
      text: isENG ? 'Airport' : 'Lotnisko',
    });

    airportFromSelect.select2('destroy');
    airportFromSelect.find('option').remove().end().select2({
      placeholder: isENG ? 'Airport' : 'Lotnisko',
      data: airportsForCountry,
    });
  });

  const countryToSelect = $(`.airport-country-${id}.to`).select2({
    data: _airportsCountries__WEBPACK_IMPORTED_MODULE_0__.default,
    placeholder: isENG ? 'Country' : 'Kraj',
  });
  const airportToSelect = $(`.airport-name-${id}.to`).select2({
    data: [],
    placeholder: isENG ? 'Airport' : 'Lotnisko',
  });

  airportToSelect.on('select2:select', e => {
    const data = e.params.data;

    airportTo = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = (airportFrom ? airportFrom.substr(0, isMobile ? 12 : 15) + '...' : '') + ' &times; ' + airportTo.substr(0, isMobile ? 12 : 15) + '...';
  });

  countryToSelect.on('select2:select', e => {
    const data = e.params.data;

    const airportsToCountry = _airports__WEBPACK_IMPORTED_MODULE_1__.default.filter(airports => airports.country === data.text);
    airportsToCountry.unshift({
      id: -1,
      text: isENG ? 'Airport' : 'Lotnisko',
    });

    airportToSelect.select2('destroy');
    airportToSelect.find('option').remove().end().select2({
      placeholder: isENG ? 'Airport' : 'Lotnisko',
      data: airportsToCountry,
    });
  });

  if (id < 11) {
    const planeImage = document.createElement('img');
    planeImage.src = './img/plane_small.png';
    planeImage.style.top = (id % 2 ? ((id - 1) * 90 + 60) : (id * 90)) + 'px'

    document.querySelector("[data-slide='trav3'] > .uk-flex > div:first-of-type").appendChild(planeImage);
  }
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (createFlight);

/***/ })

}]);
//# sourceMappingURL=createFlight.bundle.js.map