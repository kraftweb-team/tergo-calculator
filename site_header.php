<?php

?>

<!DOCTYPE html>
<html>


    <head>

        <link rel="icon" href="/favicon.ico" />

        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

	    <meta name="keywords" content="
			Carbon credits, Carbon offset, Offset, Carbon footprint, Offsetting, Carbon offsetting,Climate change,
			Terra Credits, Terra, Environmental App, Environmental, Environmental sustainability, Environmental tools,
			Environmental education tool, Environmentally conscious, CO2 emissions, Carbon dioxide emissions, CO2 emission,
			Carbon dioxide emission, Global warming, Help climate change
		" />

	    <link rel="stylesheet" href="https://use.typekit.net/jll3xbu.css">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;700;800;900&display=swap&subset=latin-ext" rel="stylesheet">
	    <link href="https://fonts.googleapis.com/css2?family=Karla:wght@400;700&display=swap" rel="stylesheet">
	    <link rel="stylesheet" href="https://use.typekit.net/xlg4tly.css">

        <title>TerGo - Register Now!</title>

        <link rel="stylesheet" type="text/css" href="../../assets/libs/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="../../assets/css/app.css" />

        <script src="../../assets/libs/jquery-3.5.1.min.js"></script>
        <script src="../../assets/libs/bootstrap/js/bootstrap.min.js"></script>

    </head>


    <body>


        <header class="container-fluid temporary-page-header-container">
            <div class="container">
                <div id="site-header" class="row">

	                <div id="page-logo" class="col-12 text-center">
		                <a href="/" title="TerGo">
			                <img
				                src="/images/new/logo_n.png"
				                alt="TerGo"
				                class="page-header-logo"
			                />
			                <span class="first">ter</span>
			                <span class="last">go</span>
		                </a>
	                </div>

                </div>
            </div>
        </header>


        <main class="container-fluid temporary-page-content-container">

            <div class="container">
                <div id="site-content" class="row">





