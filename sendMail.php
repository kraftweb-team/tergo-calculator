<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'vendor/autoload.php';

if (isset($_POST['user_email']) && isset($_POST['message_text'])) {
  $mail = new PHPMailer;

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'hello@tergo.io';                 // SMTP username
  $mail->Password = 'Terracredits';                           // SMTP password
  $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to

  $mail->setFrom($_POST['user_email']);
  $mail->addAddress('hello@tergo.io', 'Hello @TerGo');     // Add a recipient
  $mail->addReplyTo($_POST['user_email']);

  $mail->Subject = 'Tergo Contact Form';
  $mail->Body = '<p>From: ' . $_POST['user_email'] . '</p>
  <p>' . $_POST['message_text'] . '</p>';

  $mail->IsHTML(true);

  if(!$mail->send()) {
    echo 'Message could not be sent.';
    //echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
    echo '1';
  }
}