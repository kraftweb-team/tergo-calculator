import Pristine from "pristinejs";

let defaultConfig = {
  // class of the parent element where the error/success class is added
  classTo: 'form-wrapper',
  errorClass: 'has-danger',
  successClass: 'has-success',
  // class of the parent element where error text element is appended
  errorTextParent: 'form-wrapper',
  // type of element to create for the error text
  errorTextTag: 'div',
  // class of the error text element
  errorTextClass: 'text-help'
};

document.querySelectorAll('.uk-input').forEach(input => {
  if (input.getAttribute('name') === 'email' || input.getAttribute('name') === 'username' || input.getAttribute('type') === 'password') {
    input.setAttribute('data-pristine-required-message', 'To pole jest wymagane');
  }

  if (input.getAttribute('name') === 'email' || input.getAttribute('name') === 'username') {
    input.setAttribute('data-pristine-type', 'email');
    input.setAttribute('data-pristine-email-message', 'Błędny adres e-mail');
  }

  if (input.getAttribute('name') === 'password2') {
    input.setAttribute('data-pristine-equals', '#id_password');
    input.setAttribute('data-pristine-equals-message', 'Hasła nie są identyczne');

    /*
    pristine.addValidator(input, value => {
      if (value === document.querySelector('#id_password').innerHTML){
        return true;
      }
      return false;
    }, 'Hasła nie są identyczne', 1, true);
    */
  }

  input.addEventListener('focusin', () => {
    input.parentNode.classList.add('focus');

    if (input.getAttribute('name') === 'email') {
      input.parentNode.querySelector('label').innerHTML = ' E-mail - upewnij się, że jest prawidłowy!';
    }
  });
});

const pristine = new Pristine(document.querySelector('form'), defaultConfig);

const pattern = new RegExp(
  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).+$"
);

document.querySelectorAll('.uk-input').forEach(input => {
  if (input.getAttribute('type') === 'password') {
    pristine.addValidator(input, value => {
      if (pattern.test(value)){
        return true;
      }
      return false;
    }, 'min. 8 znaków | wielka litera | mała litera | cyfra', 2, false);

    // input.setAttribute('data-pristine-minlength', '8');
    // input.setAttribute('data-pristine-minlength-message', 'min. 8 znaków');
  }
});

document.querySelector('form').addEventListener('submit', e => {
  if (!pristine.validate()) {
    e.preventDefault();
  }
});
