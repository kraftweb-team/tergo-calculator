import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';

UIkit.use(Icons);

import airports from './airports';

import noUiSlider from 'nouislider';
import wNumb from 'wNumb';

import Pristine from "pristinejs";

const isENG = document.body.classList.contains('eng');

const total = {};

window.total = total;

let activeTabID = 0;
let activeSlideID = 1;

let activeSlide;

const tabsConfig = {
  0: {
    count: 27,
    uncompleted: [],
    completed: [],
    hidden: [],
  },
  1: {
    count: 8,
    uncompleted: [],
    completed: [],
    hidden: [],
  },
  2: {
    count: 6,
    uncompleted: [],
    completed: [],
    hidden: [],
  },
  3: {
    count: 17,
    uncompleted: [],
    completed: [],
    hidden: [],
  },
}

const isMobile = window.isMobile && window.isMobile.any;

if (!isMobile) {
  document.querySelector('.tab-items').classList.add('uk-switcher');

  UIkit.tab('#tabs', {
    animation: 'uk-animation-fade',
  });
} else {
  UIkit.accordion('.tab-items');

  UIkit.util.on('.tab-items', 'hide', (e) => {
    e.target.querySelector('.uk-accordion-title [uk-icon]').setAttribute('uk-icon', 'plus');
  });

  UIkit.util.on('.tab-items', 'show', (e) => {
    activeTabID = parseInt(e.target.getAttribute('data-tab'));

    e.target.querySelector('.uk-accordion-title [uk-icon]').setAttribute('uk-icon', 'minus');

    window.scrollTo({
      left: 0,
      top: document.querySelector(`.tab-items [data-tab="${activeTabID}"]`).getBoundingClientRect().top + window.scrollY,
      behavior: 'smooth',
    });
  });
}

document.querySelectorAll('#tabs li').forEach(li => {
  li.addEventListener('click', () => {
    //document.querySelector('.tab-items > li.uk-active .question-slide--active').classList.remove('question-slide--active');

    activeTabID = parseInt(li.getAttribute('data-tab-id'));

    document.querySelectorAll('.tab-items > li')[activeTabID].classList.add('uk-active');

    const activeSlide = document.querySelectorAll('.tab-items > li')[activeTabID].querySelector('.question-slide--active');

    activeSlideID = getSlideID(activeSlide);

    activeSlide.classList.remove('uk-animation-reverse');
    activeSlide.classList.add('uk-animation-fade');
  });
});

const sliders = document.querySelectorAll('.nouislider');

sliders.forEach(slider => {
  const minValue = parseInt(slider.getAttribute('data-min')) || 0;
  const maxValue = parseInt(slider.getAttribute('data-max')) || 10;
  const stepValue = parseInt(slider.getAttribute('data-step')) || 1;
  const pipsValue = parseInt(slider.getAttribute('data-pips')) || 10;

  noUiSlider.create(slider, {
    range: {
      'min': minValue,
      'max': maxValue,
    },
    step: stepValue,
    start: 0,
    tooltips: wNumb({
      decimals: 0
    }),
    pips: {
      mode: 'positions',
      values: [0, 100],
      density: pipsValue,
    }
  });
});

document.querySelectorAll('.one-value .uk-input, .one-value .uk-radio, .one-value .uk-checked, .one-value .nouislider').forEach(input => {
  input.addEventListener('input', () => {
    document.querySelectorAll(`[name="${input.name}"]`).forEach(input2 => {
      if (input !== input2) {
        if (input2.type === "number") {
          input2.value = '';
        } else if (input2.type === "radio" || input2.type === "checked") {
          input2.checked = false;
        } else if (input2.classList.contains('nouislider')) {
          input2.noUiSlider.set(input2.querySelector('.noUi-handle').getAttribute('aria-valuemax'));
        }
      }
    });
  });
});

document.querySelectorAll('.one-value .nouislider').forEach(slider => {
  slider.noUiSlider.on('change.one', () => {
    document.querySelectorAll(`[name="${slider.getAttribute('name')}"]`).forEach(input => {
      if (slider !== input) {
        if (input.type === "number") {
          input.value = '';
        } else if (input.type === "radio" || input.type === "checked") {
          input.checked = false;
        }
      }
    });
  });
});

const h26Checkboxes = document.querySelectorAll('input[name="h26"][type="checkbox"]');
const h26Radios = document.querySelectorAll('input[name="h26"][type="radio"]');

h26Checkboxes.forEach(checkbox => {
  checkbox.addEventListener('click', () => {
    h26Radios.forEach(radio => radio.checked = false);
  });
});

h26Radios.forEach(radio => {
  radio.addEventListener('click', () => {
    if (radio.value === 'none') {
      h26Checkboxes.forEach(checkbox => checkbox.checked = false);
    } else if (radio.value === 'all') {
      h26Checkboxes.forEach(checkbox => checkbox.checked = true);
    }
  });
});

const tran1Checkboxes = document.querySelectorAll('input[name="tran1"][type="checkbox"]');

tran1Checkboxes.forEach(checkbox => {
  checkbox.addEventListener('click', () => {
    if (checkbox.value === 'none') {
      tran1Checkboxes.forEach(checkbox => checkbox.checked = false);

      checkbox.checked = true;
    } else {
      tran1Checkboxes[tran1Checkboxes.length - 1].checked = false;
    }
  });
});

const h6Inputs = document.querySelectorAll('input[name="h6"]');

h6Inputs.forEach(input => {
  input.addEventListener('click', () => {
    if (input.value === 'average') {
      h6Inputs.forEach(input => {
        if (input.type === 'number') {
          input.value = '';
        };
      });
    } else {
      h6Inputs[h6Inputs.length - 1].checked = false;
    }
  });
});

/*
const h8Inputs = document.querySelectorAll('input[name="h8"]');

h8Inputs.forEach(input => {
  input.addEventListener('click', () => {
    if (input.value === 'average') {
      h8Inputs.forEach(input => {
        if (input.type === 'number') {
          input.value = '';
        };
      });
    } else {
      h8Inputs[h8Inputs.length - 1].checked = false;
    }
  });
});
*/

const activeTabClass = !isMobile ? '.uk-active' : '.uk-open';

const nextQuestions = document.querySelectorAll('.next');
const previousQuestions = document.querySelectorAll('.previous');

const updateNavButtons = (previousButton, nextButton, slide) => {
  if (activeTabID === 0) {
    if (slide.classList.contains('first')) {
      previousButton.classList.add('uk-invisible');
    } else {
      previousButton.classList.remove('uk-invisible');
    }

    if (slide.classList.contains('last') && previousButton.classList.contains('uk-invisible')) {
      previousButton.classList.remove('uk-invisible');
    }
  }

  if (activeTabID === 3 && slide.classList.contains('last')) {
    nextButton.classList.add('uk-invisible');
  } else {
    nextButton.classList.remove('uk-invisible');
  }
};

const updateProgressBar = (value) => {
  const carbonFootprintNode = document.querySelector('.carbon-footprint');

  let totalSum = 0;

  Object.keys(total).forEach(item => {
    if (!isNaN(total[item])) {
      totalSum += total[item];
    }
  });

  if (totalSum > 0) {
    carbonFootprintNode.querySelector('.value').innerHTML = totalSum.toFixed(2) + ' kg CO<sub>2</sub>';
    carbonFootprintNode.classList.remove('uk-hidden');
  } else {
    carbonFootprintNode.classList.add('uk-hidden');
  }

  if (isMobile) {
    return false;
  }

  const tabConfig = tabsConfig[activeTabID];
  const {
    uncompleted,
    completed,
    hidden,
  } = tabConfig;

  const progressBar = document.querySelectorAll('.progress-bar')[activeTabID];

  const completeMonit = document.querySelectorAll('.progress-bar')[activeTabID].querySelector('.progress-bar__complete-tooltip');

  if (!value) {
    if (uncompleted.indexOf(activeSlideID) < 0 && hidden.indexOf(activeSlideID) < 0) {
      uncompleted.push(activeSlideID);
      uncompleted.sort((a, b) => a - b);
    }

    completed.splice(completed.indexOf(activeSlideID), 1);
  } else {
    if (uncompleted.indexOf(activeSlideID) > -1) {
      uncompleted.splice(uncompleted.indexOf(activeSlideID), 1);
    }

    if (completed.indexOf(activeSlideID) < 0) {
      completed.push(activeSlideID);
      completed.sort((a, b) => a - b);
    }
  }

  progressBar.querySelector('.progress-bar__inner div').style.width = (completed.length / (tabConfig.count - tabConfig.hidden.length)) * 100 + '%';

  if (uncompleted.length) {
    completeMonit.style.display = "";

    completeMonit.innerHTML = 'complete:';

    uncompleted.forEach((item, index) => {
      const node = document.createElement('div');
      node.classList.add(item);
      node.innerHTML = item + (index === uncompleted.length - 1 ? '' : ',');
      completeMonit.appendChild(node);

      node.addEventListener('click', () => {
        const activeTabItems = document.querySelector(`.tab-items > li${activeTabClass}`);

        activeSlide = activeTabItems.querySelector('.question-slide--active');

        let nextSlide;

        activeSlide.classList.add('uk-animation-fade', 'uk-animation-reverse');
        activeSlide.classList.remove('question-slide--active');

        activeTabID = parseInt(progressBar.getAttribute('data-tab'));
        activeSlideID = item;

        removeTabActiveSlide();

        if (!isMobile) {
          UIkit.tab('.uk-tab').show(activeTabID);
        } else {
          UIkit.accordion('.tab-items').toggle(activeTabID);
        }

        const activeTab = document.querySelectorAll('.tab-items > li')[activeTabID];

        nextSlide = activeTab.querySelector(`.question-slide--${activeSlideID}`);

        nextSlide.classList.remove('uk-animation-reverse');
        nextSlide.classList.add('question-slide--active', 'uk-animation-fade');

        const [previousButton, nextButton] = activeTab.querySelectorAll('.previous, .next');

        updateNavButtons(previousButton, nextButton, nextSlide);
      });
    });
  } else {
    completeMonit.style.display = "none";
  }
};

const findSlide = (currentSlide, direction = 'next') => {
  let sibling = direction === 'next' ? currentSlide.nextElementSibling : currentSlide.previousElementSibling;

  if (!sibling) {
    return false;
  }

  while (sibling) {
    if (!sibling.classList.contains('hidden')) {
      return sibling
    }

    sibling = direction === 'next' ? sibling.nextElementSibling : sibling.previousElementSibling;
  }
};

const getSlideID = slide => {
  const dataAttribute = slide.getAttribute('data-slide');

  return parseInt(dataAttribute.match(/\d+/)[0]);
}

const removeTabActiveSlide = () => {
  const tabActiveSlide = document.querySelectorAll('.tab-items > li')[activeTabID].querySelector('.question-slide--active');

  if (tabActiveSlide) {
    tabActiveSlide.classList.remove('question-slide--active');
  }
};

const nextQuestionEvent = () => {
  const activeTabItems = document.querySelector(`.tab-items > li${activeTabClass}`);

  activeSlide = activeTabItems.querySelector('.question-slide--active');
  activeSlideID = getSlideID(activeSlide); //parseInt(activeSlide.getAttribute('data-slide').replace(/h|tran|trav|l/, ''));

  getSlideValues[activeSlide.getAttribute('data-slide')]();

  let nextSlide = findSlide(activeSlide, 'next');

  if (activeSlide.classList.contains('last') || !nextSlide) {
    activeTabID += 1;

    removeTabActiveSlide();

    nextSlide = document.querySelectorAll('.tab-items > li')[activeTabID].querySelector('.question-slide.first');
    activeSlideID = 1;

    nextSlide.classList.remove('uk-animation-reverse');
    nextSlide.classList.add('question-slide--active');
    activeSlide.classList.remove('uk-animation-fade', 'uk-animation-reverse');

    if (!isMobile) {
      UIkit.tab('.uk-tab').show(activeTabID);
    } else {
      UIkit.accordion('.tab-items').toggle(activeTabID);
    }
  } else {
    activeSlideID = getSlideID(nextSlide);
    activeSlide.classList.remove('question-slide--active');
    activeSlide.classList.add('uk-animation-fade', 'uk-animation-reverse');

    nextSlide.classList.remove('uk-animation-reverse');
    nextSlide.classList.add('question-slide--active', 'uk-animation-fade');
  }

  const [previousButton, nextButton] = activeTabItems.querySelectorAll('.previous, .next');

  updateNavButtons(previousButton, nextButton, nextSlide);
};

const getLastNotHiddenSlide = () => {
  const notHiddenSlides = document.querySelectorAll('.tab-items > li')[activeTabID].querySelectorAll('.question-slide:not(.hidden)');

  return notHiddenSlides[notHiddenSlides.length - 1];
};

const previousQuestionEvent = () => {
  let activeTabItems = document.querySelector(`.tab-items > li${activeTabClass}`);

  activeSlide = activeTabItems.querySelector('.question-slide--active');
  activeSlideID = getSlideID(activeSlide); //parseInt(activeSlide.getAttribute('data-slide').replace(/h|tran|trav|l/, ''));

  let prevSlide = findSlide(activeSlide, 'prev');

  getSlideValues[activeSlide.getAttribute('data-slide')]();

  if (activeSlide.classList.contains('first') || !prevSlide) {
    activeTabID -= 1;

    removeTabActiveSlide();

    prevSlide = getLastNotHiddenSlide();
    activeSlideID = getSlideID(prevSlide);

    prevSlide.classList.add('question-slide--active');
    activeSlide.classList.remove('uk-animation-fade', 'uk-animation-reverse');

    if (!isMobile) {
      UIkit.tab('.uk-tab').show(activeTabID);
    } else {
      UIkit.accordion('.tab-items').toggle(activeTabID);
    }
  } else {
    activeSlideID = getSlideID(prevSlide);

    activeSlide.classList.add('uk-animation-fade', 'uk-animation-reverse');
    activeSlide.classList.remove('question-slide--active');

    prevSlide.classList.remove('uk-animation-reverse');
    prevSlide.classList.add('question-slide--active', 'uk-animation-fade');
  }

  activeTabItems = document.querySelectorAll('.tab-items > li')[activeTabID];

  const [previousButton, nextButton] = activeTabItems.querySelectorAll('.previous, .next');

  updateNavButtons(previousButton, nextButton, prevSlide);
};

nextQuestions.forEach(button => {
  button.addEventListener('click', nextQuestionEvent);
});

previousQuestions.forEach(button => {
  button.addEventListener('click', previousQuestionEvent);
});

const plusButtons = document.querySelectorAll('.plus');
const minusButtons = document.querySelectorAll('.minus');

plusButtons.forEach(plusButton => {
  plusButton.addEventListener('click', () => {
    const input = plusButton.parentNode.querySelector('input');

    input.value = parseInt(input.value) + 1;
  });
});

minusButtons.forEach(minusButton => {
  minusButton.addEventListener('click', () => {
    const input = minusButton.parentNode.querySelector('input');

    if (input.value > 0) {
      input.value = parseInt(input.value) - 1;
    }
  });
});

const transportForms = document.querySelectorAll('#transportation .question-slide.first .uk-form-controls input');
const transportFormsImages = document.querySelectorAll('#transportation .question-slide.first img');

transportForms.forEach(type => {
  type.addEventListener('mouseenter', () => {
    transportFormsImages.forEach(image => {
      if (image.classList.contains(type.getAttribute('data-name'))) {
        image.classList.add('hover');
      } else {
        image.classList.remove('hover');
      }
    });
  });

  type.addEventListener('mouseleave', () => {
    transportFormsImages.forEach(image => {
      if (image.classList.contains(type.getAttribute('data-name'))) {
        image.classList.remove('hover');
      }
    });
  });

  type.addEventListener('click', () => {
    transportFormsImages.forEach(image => {
      if (image.classList.contains(type.getAttribute('data-name'))) {
        if (image.classList.contains('active')) {
          image.classList.remove('active');
        } else {
          image.classList.add('active');
        }
      }
    });
  });
});

const getInputValueByName = (inputName, targetNode = document) => {
  const inputs = targetNode.querySelectorAll(inputName);

  const valuesArray = [];

  inputs.forEach(input => {
    if (input.classList.contains('nouislider')) {
      valuesArray.push({
        value: parseInt(input.querySelector('.noUi-handle').getAttribute("aria-valuenow")),
        type: input.getAttribute("data-type"),
        formType: input.type,
      });
    } else if (input.nodeName === 'SELECT') {
      if (input.selectedIndex > 0) {
        valuesArray.push({
          value: input.selectedOptions[0].innerText,
          type: input.getAttribute("data-type"),
          formType: input.type,
        });
      }
    } else if (input.type === 'radio' || input.type === 'checkbox') {
      if (input.checked) {
        valuesArray.push({
          value: input.value,
          type: input.getAttribute("data-type"),
          formType: input.type,
        });
      }
    } else if (input.type === 'number') {
      if (input.value && input.value !== "") {
        valuesArray.push({
          value: input.value,
          type: input.getAttribute("data-type"),
          formType: input.type,
        });
      }
    }
  });

  return valuesArray;
};

let CL_h;
let Wm_t; // washing machine type
let Lo_w;
let Dm_t; // dryer machine type
let Lo_d;
let Cool_d;
let heat_d;
let Shwr_fr;
let shwr_t;
let bath_y;
let bath_fr;
let Cok_hr;
let Cok_ft;
let Cok_bn;
let wst_re;
let wst_sep;

let cur; // lifestyle question 1
let dit_ty; // lifestyle question 2
let gros_cur; // lifestyle question 3
let tkot_cur; // lifestyle question 4

let plu_y; // lifestyle question 13

let elec1;

$(document).ready(function () {
  const countryArray = [];

  import(
    /* webpackChunkName: "elec" */
    /* webpackMode: "lazy" */
    './elec'
  ).then(({
    default: elec
  }) => {
    elec1 = elec;

    Object.keys(elec).forEach((name, index) => {
      countryArray.push({
        id: index,
        text: name
      });
    });

    $('.select2.country').select2({
      data: countryArray,
      placeholder: isENG ? 'Country' : 'Kraj',
    });
  }).catch(error => {});

  $('.select2.currency').select2({
    placeholder: isENG ? 'Currency' : 'Waluta',
  });
});

const getSlideValues = {};

let EFv;

getSlideValues.h1 = () => {
  const inputValue = getInputValueByName('[name="h1"]')[0];

  if (inputValue) {
    EFv = elec1[inputValue.value];
    document.querySelector('[data-slide="h1"] .uk-fieldset').classList.remove('invalid');
  } else {
    document.querySelector('[data-slide="h1"] .uk-fieldset').classList.add('invalid');
  }

  console.log(`[h1] EFv: ${EFv}`);

  updateProgressBar(EFv);
};

let ad_N = 0;
let teen_N = 0;
let chil_N = 0;
let Per_N = 0;

getSlideValues.h2 = () => {
  getInputValueByName('[name^="h2"]').forEach(item => {
    if (item.type === "adults") {
      ad_N = parseInt(item.value);
    } else if (item.type === "teens") {
      teen_N = parseInt(item.value);
    } else if (item.type === "children") {
      chil_N = parseInt(item.value);
    }
  });

  Per_N = ad_N + teen_N + (chil_N * 0.5);

  console.log(`[h2] ad_N: ${ad_N}, teen_N: ${teen_N}, chil_N: ${chil_N}, Per_N: ${Per_N}`);

  updateProgressBar(Per_N);
};

let ho_ar = 0;

getSlideValues.h3 = () => {
  let unit;

  getInputValueByName('[name="h3"]').forEach(item => {
    if (item.type === "amount") {
      ho_ar = parseInt(item.value);
    } else if (item.type === "unit") {
      unit = item.value;
    }
  });

  if (unit === 'sq-ft') {
    ho_ar = 0.09290304 * ho_ar;
  }

  console.log(`[h3] unit: ${unit}, ho_ar: ${ho_ar}`);

  updateProgressBar(ho_ar && unit);
};

let re_b;

getSlideValues.h4 = () => {
  const inputValue = getInputValueByName('[name="h4"]')[0];

  if (inputValue) {
    re_b = inputValue.value;
  }

  console.log(`[h4] re_b: ${re_b}`);

  toggleHiddenClass(document.querySelector('[data-slide="h5"]'), re_b === 'yes');

  updateProgressBar(re_b);
};

let re_p;

getSlideValues.h5 = () => {
  const inputValue = getInputValueByName('[name="h5"]')[0];

  if (inputValue) {
    re_p = inputValue.value;
  }

  console.log(`[h5] re_p: ${re_p}`);

  updateProgressBar(re_p);
};

getSlideValues.h6 = () => {
  total.h6 = 0;

  let electricityConsumption;
  let oilConsumption;
  let gasConsumption;

  let takeAverage;

  getInputValueByName('[name="h6"]').forEach(item => {
    if (item.formType === 'number') {
      if (item.type === 'electricity') {
        electricityConsumption = parseInt(item.value);
      } else if (item.type === 'oil') {
        oilConsumption = parseInt(item.value);
      } else if (item.type === 'gas') {
        gasConsumption = parseInt(item.value);
      }
    } else if (item.value === 'average') {
      takeAverage = true;
    }
  });

  if (takeAverage) {
    total.h6 += (10.425 * EFv * ho_ar) / Per_N;
  } else {
    if (oilConsumption) {
      total.h6 += (oilConsumption * 2.54042) / Per_N;
    }

    if (gasConsumption) {
      total.h6 += (gasConsumption * 2.03053) / Per_N;
    }

    if (electricityConsumption) {
      if (re_b === 'yes') {
        if (re_p === '0%') {
          total.h6 += (electricityConsumption * EFv) / Per_N;
        } else if (re_p === '25%') {
          total.h6 += ((electricityConsumption * EFv * 0.75) + (electricityConsumption * 0.022 * 0.25)) / Per_N;
        } else if (re_p === '50%') {
          total.h6 += ((electricityConsumption * EFv * 0.5) + (electricityConsumption * 0.022 * 0.5)) / Per_N;
        } else if (re_p === '75%') {
          total.h6 += ((electricityConsumption * EFv * 0.25) + (electricityConsumption * 0.022 * 0.75)) / Per_N;
        } else if (re_p === '100%') {
          total.h6 += (electricityConsumption * 0.022 * 0.75) / Per_N;
        }
      } else {
        if (electricityConsumption) {
          total.h6 += (electricityConsumption * EFv) / Per_N;
        }
      }
    }
  }

  console.log(`[h6] total: ${total.h6}`);

  updateProgressBar(total.h6);
};

getSlideValues.h7 = () => {
  let electricityConsumption;

  total.h7 = 0;

  const averageConsumption = {
    low: 175,
    medium: 250,
    hight: 400,
  }

  getInputValueByName('[name="h7"]').forEach(item => {
    if (item.formType === 'radio') {
      electricityConsumption = averageConsumption[item.value];
    } else if (item.formType === 'number') {
      electricityConsumption = parseInt(item.value);
    } else {
      total.h7 = 10.425 * EFv * ho_ar;
    }
  });

  if (re_b === 'yes') {
    if (re_p === '0%') {
      total.h7 = (EFv * electricityConsumption) / Per_N;
    } else if (re_p === '25%') {
      total.h7 = ((EFv * electricityConsumption * 0.75) + (electricityConsumption * 0.022 * 0.25)) / Per_N;
    } else if (re_p === '50%') {
      total.h7 = ((EFv * electricityConsumption * 0.5) + (electricityConsumption * 0.022 * 0.5)) / Per_N;
    } else if (re_p === '75%') {
      total.h7 = ((EFv * electricityConsumption * 0.25) + (electricityConsumption * 0.022 * 0.75)) / Per_N;
    } else if (re_p === '100%') {
      total.h7 = (electricityConsumption * 0.022) / Per_N;
    }
  } else {
    total.h7 = (EFv * electricityConsumption) / Per_N;
  }

  console.log(`[h7] re_b: ${re_b}, electricityConsumption: ${electricityConsumption}, total: ${total.h7}`);

  updateProgressBar(total.h7);
};

let Wh_t;
let Wh_d;
let Wh_dt;

getSlideValues.h8 = () => {
  total.h8 = 0;

  const inputValue = getInputValueByName('[name="h8"]')[0];

  Wh_dt = (getInputValueByName('[name="h8-unit"]') && getInputValueByName('[name="h8-unit"]')[0] && getInputValueByName('[name="h8-unit"]')[0].value) || 'C';

  let takeAverage;

  if (inputValue) {
    if (inputValue.value === 'average') {
      Wh_t = 'electric';
      Wh_d = 75;
      
      takeAverage = true;
    } else {
      Wh_t = inputValue.type;
      Wh_d = inputValue.value;
    }
  }

  if (takeAverage) {
    total.h8 += EFv * 11.44 * 4;
  } else {
    if (re_b === 'no') {
      if (Wh_t === 'solar' && Wh_dt === 'C') {
        total.h8 += Wh_d * 0 * EFv;
      } else if (Wh_t === 'solar' && Wh_dt === 'F') {
        total.h8 += Wh_d * 0 * EFv;
      } else if (Wh_t === 'electric' && Wh_dt === 'C') {
        total.h8 += (Wh_d - 22) * 0.22 * EFv * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'F') {
        total.h8 += (Wh_d - 71.6) * 0.12 * EFv * 4;
      } else if (Wh_t === 'gas' && Wh_dt === 'C') {
        total.h8 += (Wh_d - 22) * 0.042 * 4;
      } else if (Wh_t === 'gas' && Wh_dt === 'F') {
        total.h8 += (Wh_d - 71.6) * 0.0234 * 4;
      }
    } else {
      if (Wh_t === 'solar' && Wh_dt === 'C') {
        total.h8 += Wh_d * 0 * EFv;
      } else if (Wh_t === 'solar' && Wh_dt === 'F') {
        total.h8 += Wh_d * 0 * EFv;
      } else if (Wh_t === 'electric' && Wh_dt === 'C' && re_p === '0%') {
        total.h8 += (Wh_d - 22) * 0.22 * EFv * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'C' && re_p === '25%') {
        total.h8 += (((Wh_d - 22) * 0.22 * EFv * 0.75) + ((Wh_d - 22) * 0.22 * 0.022 * 0.25)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'C' && re_p === '50%') {
        total.h8 += (((Wh_d - 22) * 0.22 * EFv * 0.5) + ((Wh_d - 22) * 0.22 * 0.022 * 0.5)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'C' && re_p === '75%') {
        total.h8 += (((Wh_d - 22) * 0.22 * EFv * 0.25) + ((Wh_d - 22) * 0.22 * 0.022 * 0.75)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'C' && re_p === '100%') {
        total.h8 += (Wh_d - 22) * 0.22 * 0.022 * 4;
      } if (Wh_t === 'electric' && Wh_dt === 'F' && re_p === '0%') {
        total.h8 += (Wh_d - 71.6) * 0.12 * EFv * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'F' && re_p === '25%') {
        total.h8 += (((Wh_d - 71.6) * 0.12 * EFv * 0.75) + ((Wh_d - 71.6) * 0.12 * 0.022 * 0.25)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'F' && re_p === '50%') {
        total.h8 += (((Wh_d - 71.6) * 0.12 * EFv * 0.5) + ((Wh_d - 71.6) * 0.12 * 0.022 * 0.5)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'F' && re_p === '75%') {
        total.h8 += (((Wh_d - 71.6) * 0.12 * EFv * 0.25) + ((Wh_d - 71.6) * 0.12 * 0.022 * 0.75)) * 4;
      } else if (Wh_t === 'electric' && Wh_dt === 'F' && re_p === '100%') {
        total.h8 += (Wh_d - 71.6) * 0.12 * 0.022 * 4;
      } else if (Wh_t === 'gas' && Wh_dt === 'C') {
        total.h8 += (Wh_d - 22) * 0.0420 * 4;
      } else if (Wh_t === 'gas' && Wh_dt === 'F') {
        total.h8 += (Wh_d - 71.6) * 0.0234 * 4;
      }
    }
  }

  console.log(`[h8] Wh_t: ${Wh_t}, Wh_d: ${Wh_d}, Wh_dt: ${Wh_dt}, total: ${total.h8}`);

  updateProgressBar(Wh_t && Wh_d && Wh_dt);
};

getSlideValues.h9 = () => {
  const inputValue = getInputValueByName('[name="h9"]')[0];

  if (inputValue) {
    Wm_t = inputValue.value;
  }

  console.log(`[h9] Wm_t: ${Wm_t}`);

  updateProgressBar(Wm_t);
};

getSlideValues.h10 = () => {
  const inputValue = getInputValueByName('[name="h10"]')[0];

  if (inputValue) {
    Lo_w = parseInt(inputValue.value);

    const washingMachines = {
      compact: 0.6988,
      'top-load': 0.59897,
      'front-load': 0.6988,
      'extra-large': 0.74871,
    }

    total.h10 = (Lo_w * washingMachines[Wm_t] * 4) / Per_N;
  }

  console.log(`[h10] Lo_w: ${Lo_w}, total: ${total.h10}`);

  updateProgressBar(Lo_w);
};

getSlideValues.h11 = () => {
  const inputValue = getInputValueByName('[name="h11"]')[0];

  if (inputValue) {
    Dm_t = inputValue.value;
  }

  console.log(`[h11] Dm_t: ${Dm_t}`);

  updateProgressBar(Dm_t);
};

getSlideValues.h12 = () => {
  const inputValue = getInputValueByName('[name="h12"]')[0];

  if (inputValue) {
    Lo_d = parseInt(inputValue.value);

    const dryerMachines = {
      compact: 0.74871,
      standard: 1.74699,
      'mega-capacity': 2.99484,
      'combo-standard': 1.04819,
    }

    total.h12 = (Lo_d * dryerMachines[Dm_t] * 4) / Per_N;
  }

  console.log(`[h12] Lo_d: ${Lo_d}, total: ${total.h12}`);

  updateProgressBar(Lo_d);
};

getSlideValues.h13 = () => {
  const inputValue = getInputValueByName('[name="h13"]')[0];

  if (inputValue) {
    Cool_d = inputValue.value;
  }

  console.log(`[h13] Cool_d: ${Cool_d}`);

  updateProgressBar(Cool_d);
};

getSlideValues.h14 = () => {
  const inputValue = getInputValueByName('[name="h14"]')[0];

  if (inputValue) {
    heat_d = inputValue.value;
  }

  console.log(`[h14] heat_d: ${heat_d}`);

  updateProgressBar(heat_d);
};

getSlideValues.h15 = () => {
  const inputValue = getInputValueByName('[name="h15"]')[0];

  if (inputValue) {
    const times = {
      none: 0,
      'once-year': 236.727,
      'twice-year': 473.454,
    };

    total.h15 = times[inputValue.value] / Per_N;
  }

  console.log(`[h15] total: ${total.h15}`);

  updateProgressBar(inputValue && inputValue.value);
};

getSlideValues.h16 = () => {
  const inputValue = getInputValueByName('[name="h16"]')[0];

  if (inputValue) {
    Shwr_fr = parseInt(inputValue.value);
  }

  console.log(`[h16] Shwr_fr: ${Shwr_fr}`);

  updateProgressBar(Shwr_fr);
};

getSlideValues.h17 = () => {
  const inputValue = getInputValueByName('[name="h17"]')[0];

  if (inputValue) {
    shwr_t = parseInt(inputValue.value);
  }

  console.log(`[h17] shwr_t: ${shwr_t}`);

  updateProgressBar(shwr_t);
};

getSlideValues.h18 = () => {
  const inputValue = getInputValueByName('[name="h18"]')[0];

  if (inputValue) {
    bath_y = inputValue.value;
  }

  toggleHiddenClass(document.querySelector('[data-slide="h19"]'), bath_y === 'yes');

  if (bath_y === 'no') {
    total.h19 = shwr_t * 0.00995 * Shwr_fr * 7 * 4;
  }

  console.log(`[h18] bath_y: ${bath_y}` + (bath_y === 'no' ? ` total: ${total.h19}` : ''));

  updateProgressBar(bath_y);
};

getSlideValues.h19 = () => {
  const inputValue = getInputValueByName('[name="h19"]')[0];

  if (inputValue) {
    bath_fr = parseInt(inputValue.value);

    total.h19 = ((shwr_t * 0.00995 * Shwr_fr * 7) + (bath_fr * 0.27878)) * 4;
  }

  console.log(`[h19] bath_fr: ${bath_fr}, total: ${total.h19}`);

  updateProgressBar(bath_fr);
};

getSlideValues.h20 = () => {
  const inputValue = getInputValueByName('[name="h20"]')[0];

  if (inputValue) {
    Cok_hr = parseInt(inputValue.value);
  }

  console.log(`[h20] Cok_hr: ${Cok_hr}`);

  updateProgressBar(Cok_hr);
};

getSlideValues.h21 = () => {
  const inputValue = getInputValueByName('[name="h21"]')[0];

  if (inputValue) {
    Cok_ft = inputValue.value;
  }

  console.log(`[h21] Cok_ft: ${Cok_ft}`);

  updateProgressBar(Cok_ft);
};

getSlideValues.h22 = () => {
  const inputValue = getInputValueByName('[name="h22"]')[0];

  if (inputValue) {
    Cok_bn = inputValue.value;

    const gasBurners = {
      1: 2.43648,
      2: 3.923748,
      3: 4.769748,
      4: 6.461748,
      5: 9.234936,
      6: 11.6748,
      7: 14.66118,
      8: 19.55106,
    };

    const electircBurners = {
      1: 0.59897,
      2: 0.84854,
      3: 1.74699,
      4: 5.19106,
      5: 5.09123,
      6: 5.79002,
    };

    total.h22 = (Cok_hr * (Cok_ft === 'gas' ? gasBurners[Cok_bn] : electircBurners[Cok_bn]) * 4) / Per_N;
  }

  console.log(`[h22] Cok_bn: ${Cok_bn}, total: ${total.h22}`);

  updateProgressBar(Cok_bn);
};

getSlideValues.h23 = () => {
  const inputValue = getInputValueByName('[name="h23"]')[0];

  if (inputValue) {
    wst_re = inputValue.value;
  }

  console.log(`[h23] wst_re: ${wst_re}`);

  updateProgressBar(wst_re);
};

getSlideValues.h24 = () => {
  const inputValue = getInputValueByName('[name="h24"]')[0];

  if (inputValue) {
    wst_sep = inputValue.value;
  }

  console.log(`[h24] wst_sep: ${wst_sep}`);

  updateProgressBar(wst_sep);
};

let wst_fr;
let wst_siz;

getSlideValues.h25 = () => {
  const inputValue = getInputValueByName('[name="h25"]')[0];

  if (inputValue) {
    [wst_siz, wst_fr] = inputValue.value.split('-');

    wst_fr = parseInt(wst_fr);

    if (wst_re === 'yes' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'small') {
      total.h25 = (((7 * wst_fr) * 0.08083) * 4) / Per_N;
    } else if (wst_re === 'no' && wst_sep === 'no' && wst_siz === 'small') {
      total.h25 = (((7 * wst_fr) * 2.37) * 4) / Per_N;
    } else if (wst_re === 'no' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'small') {
      total.h25 = (((7 * wst_fr) * 0.3776) * 4) / Per_N;
    } else if (wst_re === 'yes' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'medium') {
      total.h25 = (((25 * wst_fr) * 0.08083) * 4) / Per_N;
    } else if (wst_re === 'no' && wst_sep === 'no' && wst_siz === 'medium') {
      total.h25 = (((25 * wst_fr) * 2.37) * 4) / Per_N;
    } else if (wst_re === 'no' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'medium') {
      total.h25 = (((25 * wst_fr) * 0.3776) * 4) / Per_N;
    } else if (wst_re === 'yes' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'large') {
      total.h25 = (((32 * wst_fr) * 0.08083) * 4) / Per_N;
    } else if (wst_re === 'no' && wst_sep === 'no' && wst_siz === 'large') {
      total.h25 = (((32 * wst_fr) * 2.37) * 4) / Per_N;
    } else if (wst_re === 'no' && (wst_sep === 'all-the-time' || wst_sep === 'sometimes') && wst_siz === 'large') {
      total.h25 = (((32 * wst_fr) * 0.3776) * 4) / Per_N;
    }
  }

  console.log(`[h25] wst_siz: ${wst_siz}, total: ${total.h25}`);
};

let en_efi;

getSlideValues.h26 = () => {
  en_efi = [];

  getInputValueByName('[name="h26"]').forEach(item => {
    en_efi.push(item.value)
  });

  console.log(`[h26] en_efi: ${en_efi}`);

  updateProgressBar(en_efi.length);
};

getSlideValues.h27 = () => {
  const inputValue = getInputValueByName('[name="h27"]')[0];

  if (inputValue) {
    CL_h = inputValue.value;
  }

  console.log(`[h27] CL_h: ${CL_h}`);

  updateProgressBar(CL_h);
};

let selectedTransType = {};

const removeFromArray = (array, element) => {
  if (!(array && element)) {
    return false;
  }

  array.splice(array.indexOf(element), 1);
}

const toggleHiddenClass = (slide, condition) => {
  if (condition) {
    slide.classList.remove('hidden');

    removeFromArray(tabsConfig[activeTabID].hidden, getSlideID(slide));
  } else {
    slide.classList.add('hidden');

    tabsConfig[activeTabID].hidden.push(getSlideID(slide));
  }
};

getSlideValues.tran1 = () => {
  selectedTransType = {};

  getInputValueByName('[name="tran1"]').forEach(item => {
    selectedTransType[item.value] = [];
  });

  const selectedTransTypeKeys = Object.keys(selectedTransType);

  if (selectedTransTypeKeys.length > 0 && selectedTransTypeKeys.indexOf('none') < 0) {
    document.querySelectorAll('[data-slide="tran2"], [data-slide="tran4"]').forEach(slide => {
      toggleHiddenClass(slide, selectedTransTypeKeys.find(value => /car|taxi-uber|motorbike|moped|bike|scooter/.test(value)));
    });

    document.querySelectorAll('[data-slide="tran6"], [data-slide="tran7"]').forEach(slide => {
      toggleHiddenClass(slide, selectedTransTypeKeys.indexOf('public') > -1);
    });

    document.querySelectorAll('[data-slide="tran8"]').forEach(slide => {
      toggleHiddenClass(slide, selectedTransTypeKeys.indexOf('taxi-uber') > -1);
    });

    document.querySelectorAll('[data-slide="tran3"], [data-slide="tran5"]').forEach(slide => {
      toggleHiddenClass(slide, selectedTransTypeKeys.indexOf('car') > -1);
    });

    document.querySelectorAll('[data-slide="tran2"] input[type="number"], [data-slide="tran4"] input[type="radio"]').forEach(input => {
      if (selectedTransTypeKeys.indexOf(input.getAttribute('data-type')) < 0) {
        input.parentNode.parentNode.classList.add('uk-hidden');
      } else {
        input.parentNode.parentNode.classList.remove('uk-hidden');
      }
    });
  } else {
    document.querySelectorAll('[data-slide^="tran"]:not(.first)').forEach(slide => {
      toggleHiddenClass(slide, false);
    });
  }

  console.log(`[tran1] selectedTransType: ${Object.keys(selectedTransType)}`);

  updateProgressBar(Object.keys(selectedTransType).length);
};

let distUnit;
let timeRange;

getSlideValues.tran2 = () => {
  let checked;

  const consoleTable = [];
  consoleTable.push(['type', 'dist']);

  getInputValueByName('[name^="tran2"]').forEach(item => {
    if (item.formType === 'number' && selectedTransType[item.type]) {
      selectedTransType[item.type][0] = parseInt(item.value);

      consoleTable.push([item.type, parseInt(item.value)]);

      checked = true;
    } else if (item.type === 'dist-unit') {
      distUnit = item.value;
    } else if (item.type === 'time-range') {
      timeRange = item.value;
    }
  });

  console.log(`[tran2] distUnit: ${distUnit}, timeRange: ${timeRange}`);
  console.table(consoleTable);

  updateProgressBar(distUnit && checked);
};

let car_s;

getSlideValues.tran3 = () => {
  const inputValue = getInputValueByName('[name="tran3"]')[0];

  if (inputValue) {
    car_s = inputValue.value;
  }

  document.querySelectorAll('[data-slide="tran4"] input[data-type="car"][value="electric"], [data-slide="tran4"] input[data-type="car"][value="hybrid"]').forEach(input => {
    if (car_s === 'truck') {
      input.parentNode.classList.add('uk-hidden');
    } else {
      input.parentNode.classList.remove('uk-hidden');
    }
  });

  console.log(`[tran3] car_s: ${car_s}`);

  updateProgressBar(car_s);
};

const sumTransTotal = () => {
  total.tran5 = 0;

  const carConfig = selectedTransType.car;

  if (carConfig && carConfig.length > 1 && car_s) {
    let carTotal = 0;

    if (distUnit === 'kms') {
      if (car_s === 'small' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.14836) + (carConfig[0] * 0.04066);
      } else if (car_s === 'medium' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.18659) + (carConfig[0] * 0.05119);
      } else if (car_s === 'large' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.27807) + (carConfig[0] * 0.07638);
      } else if (car_s === 'small' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.13721) + (carConfig[0] * 0.0329);
      } else if (car_s === 'medium' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.16637) + (carConfig[0] * 0.03998);
      } else if (car_s === 'large' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.20419) + (carConfig[0] * 0.04918);
      } else if (car_s === 'small' && carConfig[1] === 'electric') {
        carTotal = (carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294);
      } else if (car_s === 'medium' && carConfig[1] === 'electric') {
        carTotal = (carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294);
      } else if (car_s === 'large' && carConfig[1] === 'electric') {
        carTotal = (carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294);
      } else if (car_s === 'small' && carConfig[1] === 'hybrid') {
        carTotal = (carConfig[0] * 0.02935) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705);
      } else if (car_s === 'medium' && carConfig[1] === 'hybrid') {
        carTotal = (carConfig[0] * 0.07083) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705);
      } else if (car_s === 'large' && carConfig[1] === 'hybrid') {
        carTotal = (carConfig[0] * 0.07429) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705);
      } else if (car_s === 'truck' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.1987) + (carConfig[0] * 0.05649);
      } else if (car_s === 'truck' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.18101) + (carConfig[0] * 0.03942);
      }
    } else {
      if (car_s === 'small' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.23877) + (carConfig[0] * 0.06544);
      } else if (car_s === 'medium' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.30029) + (carConfig[0] * 0.08238);
      } else if (car_s === 'large' && carConfig[1] === 'petrol') {
        carTotal = (carConfig[0] * 0.44752) + (carConfig[0] * 0.12292);
      } else if (car_s === 'small' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.22082) + (carConfig[0] * 0.05294);
      } else if (car_s === 'medium' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.26775) + (carConfig[0] * 0.06435);
      } else if (car_s === 'large' && carConfig[1] === 'diesel') {
        carTotal = (carConfig[0] * 0.32863) + (carConfig[0] * 0.07914);
      } else if (car_s === 'small' && carConfig[1] === 'electric') {
        carTotal = ((carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294)) / 0.621371192;
      } else if (car_s === 'medium' && carConfig[1] === 'electric') {
        carTotal = ((carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294)) / 0.621371192;
      } else if (car_s === 'large' && carConfig[1] === 'electric') {
        carTotal = ((carConfig[0] * 0) + (carConfig[0] * 0.0065) + (carConfig[0] * 0.04294)) / 0.621371192;
      } else if (car_s === 'small' && carConfig[1] === 'hybrid') {
        carTotal = ((carConfig[0] * 0.02935) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705)) / 0.621371192;
      } else if (car_s === 'medium' && carConfig[1] === 'hybrid') {
        carTotal = ((carConfig[0] * 0.07083) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705)) / 0.621371192;
      } else if (car_s === 'large' && carConfig[1] === 'hybrid') {
        carTotal = ((carConfig[0] * 0.07429) + (carConfig[0] * 0.02565) + (carConfig[0] * 0.03705)) / 0.621371192;
      } else if (car_s === 'truck' && carConfig[1] === 'petrol') {
        carTotal = ((carConfig[0] * 0.1987) + (carConfig[0] * 0.05649)) / 0.621371192;
      } else if (car_s === 'truck' && carConfig[1] === 'diesel') {
        carTotal = ((carConfig[0] * 0.18101) + (carConfig[0] * 0.03942)) / 0.621371192;
      }
    }

    if (timeRange === 'weekly') {
      carTotal = carTotal * 4;
    }

    console.log(`[tran5] car_s: ${car_s}, fuel type: ${carConfig[1]}, dist: ${carConfig[0]}, total: ${carTotal}`);

    total.tran5 += carTotal;
  }

  const tranConsoleTable = [];
  tranConsoleTable.push(['tran', 'type', 'dist', 'total']);

  const bikeConfig = selectedTransType.bike;

  if (bikeConfig && bikeConfig.length > 1) {
    let bikeTotal = 0;

    if (bikeConfig[1] === 'electric') {
      bikeTotal = (bikeConfig[0] * 0.04 * EFv) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (bikeConfig[1] === 'manual') {
      bikeTotal = bikeConfig[0] * 0;
    }

    if (timeRange === 'weekly') {
      bikeTotal = bikeTotal * 4;
    }

    tranConsoleTable.push(['bike', bikeConfig[1], bikeConfig[0], bikeTotal]);

    total.tran5 += bikeTotal;
  }

  const scooterConfig = selectedTransType.scooter;

  if (scooterConfig && scooterConfig.length > 1) {
    let scooterTotal = 0;

    if (scooterConfig[1] === 'electric') {
      scooterTotal = (scooterConfig[0] * 0.04 * EFv) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (scooterConfig[1] === 'manual') {
      scooterTotal = scooterConfig[0] * 0;
    }

    if (timeRange === 'weekly') {
      scooterTotal = scooterTotal * 4;
    }

    tranConsoleTable.push(['scooter', scooterConfig[1], scooterConfig[0], scooterTotal]);

    total.tran5 += scooterTotal;
  }

  const motorbikeConfig = selectedTransType.motorbike;

  if (motorbikeConfig && motorbikeConfig.length > 1) {
    let motorbikeTotal = 0;

    if (motorbikeConfig[1] === 'petrol') {
      motorbikeTotal = (motorbikeConfig[0] * 116.2 / 1000) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (motorbikeConfig[1] === 'diesel') {
      motorbikeTotal = (motorbikeConfig[0] * 0.0277 * 2.59411) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (motorbikeConfig[1] === 'electric') {
      motorbikeTotal = (motorbikeConfig[0] * 0.0472 * EFv) / (distUnit === 'miles' ? 0.621371192 : 1);
    }

    if (timeRange === 'weekly') {
      motorbikeTotal = motorbikeTotal * 4;
    }

    tranConsoleTable.push(['motorbike', motorbikeConfig[1], motorbikeConfig[0], motorbikeTotal]);

    total.tran5 += motorbikeTotal;
  }

  const mopedConfig = selectedTransType.moped;

  if (mopedConfig && mopedConfig.length > 1) {
    let mopedTotal = 0;

    if (mopedConfig[1] === 'petrol') {
      mopedTotal = (mopedConfig[0] * 116.2 / 1000) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (mopedConfig[1] === 'diesel') {
      mopedTotal = (mopedConfig[0] * 0.0277 * 2.59411) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (mopedConfig[1] === 'electric') {
      mopedTotal = (mopedConfig[0] * 0.0472 * EFv) / (distUnit === 'miles' ? 0.621371192 : 1);
    }

    if (timeRange === 'weekly') {
      mopedTotal = mopedTotal * 4;
    }

    tranConsoleTable.push(['moped', mopedConfig[1], mopedConfig[0], mopedTotal]);

    total.tran5 += mopedTotal;
  }

  console.table(tranConsoleTable);
};

getSlideValues.tran4 = () => {
  let checked;

  getInputValueByName('[name^="tran4"]').forEach(item => {
    if (item.formType === 'radio' && selectedTransType[item.type]) {
      selectedTransType[item.type][1] = item.value;

      checked = true;
    }
  });

  sumTransTotal();

  console.log(`[tran4] total: ${total.tran5}`);

  updateProgressBar(checked);
};

let car_pc;

getSlideValues.tran5 = () => {
  const inputValue = getInputValueByName('[name="tran5"]')[0];

  if (inputValue) {
    car_pc = inputValue.value;
  }

  sumTransTotal();

  console.log(`[tran5] car_pc: ${car_pc}, total: ${total.tran5}`);

  updateProgressBar(car_pc);
};

let pt_t = {};

getSlideValues.tran6 = () => {
  pt_t = {};

  getInputValueByName('[name="tran6"]').forEach(item => {
    pt_t[item.value] = "";
  });

  toggleHiddenClass(document.querySelector('[data-slide="tran7"]'), Object.keys(pt_t).length > 0);

  document.querySelectorAll('[data-slide="tran7"] input[type="number"]').forEach(input => {
    if (Object.keys(pt_t).indexOf(input.getAttribute('data-type')) < 0) {
      input.parentNode.parentNode.classList.add('uk-hidden');
    } else {
      input.parentNode.parentNode.classList.remove('uk-hidden');
    }
  });

  console.log(`[tran6] pt_t: ${Object.keys(pt_t)}`);

  updateProgressBar(pt_t);
};

getSlideValues.tran7 = () => {
  let distUnit;
  let timeUnit;

  let checked;

  getInputValueByName('[name^="tran7"]').forEach(item => {
    if (item.formType === 'number' && pt_t.hasOwnProperty(item.type)) {
      pt_t[item.type] = parseInt(item.value);

      checked = true;
    } else if (item.type === 'dist-unit') {
      distUnit = item.value;
    } else if (item.type === 'time-unit') {
      timeUnit = item.value;
    }
  });

  total.tran7 = 0;

  const publicTranConsoleTable = [];
  publicTranConsoleTable.push(['type', 'dist', 'total']);

  Object.keys(pt_t).forEach(type => {
    let typeTotal = 0;

    if (type === 'metro') {
      typeTotal = (pt_t[type] * 0.02472 * EFv * (timeUnit === 'weekly' ? 4 : 1)) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (type === 'tramway') {
      typeTotal = (pt_t[type] * 0.015925 * EFv * (timeUnit === 'weekly' ? 4 : 1)) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (type === 'bus') {
      typeTotal = ((pt_t[type] * 26.34 / 1000) * (timeUnit === 'weekly' ? 4 : 1)) / (distUnit === 'miles' ? 0.621371192 : 1);
    } else if (type === 'rail') {
      typeTotal = ((pt_t[type] * 39 / 1000) * (timeUnit === 'weekly' ? 4 : 1)) / (distUnit === 'miles' ? 0.621371192 : 1);
    }

    publicTranConsoleTable.push([type, pt_t[type], typeTotal]);

    total.tran7 += typeTotal;
  });

  console.log(`[tran7] distUnit: ${distUnit}, timeUnit: ${timeUnit}, total: ${total.tran7}`);
  console.table(publicTranConsoleTable);

  updateProgressBar(distUnit && timeUnit && checked);
};

let t_pc;

getSlideValues.tran8 = () => {
  const inputValue = getInputValueByName('[name="tran8"]')[0];

  if (inputValue) {
    t_pc = inputValue.value || 1;
  }

  total.tran8 = 0;

  const taxiConfig = selectedTransType['taxi-uber'];

  let taxiTotal = 0;

  if (taxiConfig && taxiConfig.length > 1) {
    if (taxiConfig[1] === 'petrol') {
      taxiTotal = (((taxiConfig[0] * 0.20792) + (taxiConfig[0] * 0.05649)) / (distUnit === 'miles' ? 0.621371192 : 1) / t_pc);
    } else if (taxiConfig[1] === 'electric') {
      taxiTotal = (((taxiConfig[0] * 0.0065) + (taxiConfig[0] * 0.04294)) / (distUnit === 'miles' ? 0.621371192 : 1) / t_pc);
    } else if (taxiConfig[1] === 'diesel') {
      taxiTotal = (((taxiConfig[0] * 0.16533) + (taxiConfig[0] * 0.03942)) / (distUnit === 'miles' ? 0.621371192 : 1) / t_pc);
    }

    if (timeRange === 'weekly') {
      taxiTotal = taxiTotal * 4;
    }

    total.tran8 += taxiTotal;
  }

  console.log(`[tran8] t_pc: ${t_pc}, total: ${total.tran8}`);

  updateProgressBar(t_pc);
};

let cal_type;

getSlideValues.trav1 = () => {
  const inputValue = getInputValueByName('[name="trav1"]')[0];

  if (inputValue) {
    cal_type = inputValue.value;

    if (cal_type === 'enter-flights') {
      document.querySelectorAll('[data-slide="trav2"], [data-slide="trav3"]').forEach(slide => {
        toggleHiddenClass(slide, true);
      });

      document.querySelector(`[data-slide="trav2"] form.${cal_type}`).style.display = 'block';
      document.querySelector('[data-slide="trav2"] form.estimate').style.display = '';

      document.querySelector('.add-flight').addEventListener('click', () => {
        const accordion = document.querySelector("[data-slide='trav3'] [uk-accordion]");

        import(
          /* webpackChunkName: "createFlight" */
          /* webpackMode: "lazy" */
          './createFlight'
        ).then(({
          default: createFlight
        }) => {
          createFlight(accordion.querySelectorAll('li').length, accordion);
        }).catch(error => {});
      });

    } else if (cal_type === 'estimate') {
      toggleHiddenClass(document.querySelector('[data-slide="trav2"]'), true);
      toggleHiddenClass(document.querySelector('[data-slide="trav3"]'), false);

      document.querySelector('[data-slide="trav2"] form.estimate').style.display = 'block';
      document.querySelector('[data-slide="trav2"] form.enter-flights').style.display = '';

    } else if (cal_type === 'no-flight') {
      document.querySelectorAll('[data-slide="trav2"], [data-slide="trav3"]').forEach(slide => {
        toggleHiddenClass(slide, false);
      });
    }
  } else {
    document.querySelectorAll('[data-slide="trav2"], [data-slide="trav3"]').forEach(slide => {
      toggleHiddenClass(slide, false);
    });
  }

  console.log(`[trav1] cal_type: ${cal_type}`);

  updateProgressBar(cal_type);
};

let fl_c;

getSlideValues.trav2 = () => {
  if (cal_type === 'estimate') {
    let flyWithinEurope;
    let flyOutsideEurope;

    getInputValueByName('[name="trav2"]').forEach(item => {
      if (item.type === 'within') {
        flyWithinEurope = parseInt(item.value);
      } else if (item.type === 'outside') {
        flyOutsideEurope = parseInt(item.value);
      }
    });

    let withinEuropeClass = getInputValueByName('[name="trav2-class-within"]')[0];
    withinEuropeClass = withinEuropeClass && withinEuropeClass.value;

    let outsideEuropeClass = getInputValueByName('[name="trav2-class-outside"]')[0];
    outsideEuropeClass = outsideEuropeClass && outsideEuropeClass.value;

    let withinEuropeDistance = getInputValueByName('[name="trav2-distance-within"]')[0];
    withinEuropeDistance = withinEuropeDistance && withinEuropeDistance.value;

    let outsideEuropeDistance = getInputValueByName('[name="trav2-distance-outside"]')[0];
    outsideEuropeDistance = outsideEuropeDistance && outsideEuropeDistance.value;

    let outsideEuropeTotal = 0;
    let withinEuropeTotal = 0;

    total.trav2 = 0;

    let trt;

    if (withinEuropeClass === 'economic' && withinEuropeDistance === 'short') {
      trt = 249.168;
    } else if (withinEuropeClass === 'economic' && withinEuropeDistance === 'medium') {
      trt = 497.71;
    } else if (withinEuropeClass === 'economic' && withinEuropeDistance === 'long') {
      trt = 586.79;
    } else if (withinEuropeClass === 'business' && withinEuropeDistance === 'short') {
      trt = 373.76;
    } else if (withinEuropeClass === 'business' && withinEuropeDistance === 'medium') {
      trt = 746.58;
    } else if (withinEuropeClass === 'business' && withinEuropeDistance === 'long') {
      trt = 880.20;
    } else if (withinEuropeClass === 'first-class' && withinEuropeDistance === 'short') {
      trt = 479.4;
    } else if (withinEuropeClass === 'first-class' && withinEuropeDistance === 'medium') {
      trt = 957.60;
    } else if (withinEuropeClass === 'first-class' && withinEuropeDistance === 'long') {
      trt = 1128.98;
    }

    withinEuropeTotal = (flyWithinEurope * trt) / 12;

    total.trav2 += withinEuropeTotal;

    if (outsideEuropeClass === 'economic' && outsideEuropeDistance === 'short') {
      trt = 906.66;
    } else if (outsideEuropeClass === 'economic' && outsideEuropeDistance === 'medium') {
      trt = 1245.22;
    } else if (outsideEuropeClass === 'economic' && outsideEuropeDistance === 'long') {
      trt = 2077.56;
    } else if (outsideEuropeClass === 'business' && outsideEuropeDistance === 'short') {
      trt = 1360.02;
    } else if (outsideEuropeClass === 'business' && outsideEuropeDistance === 'medium') {
      trt = 3611.23;
    } else if (outsideEuropeClass === 'business' && outsideEuropeDistance === 'long') {
      trt = 6025.09;
    } else if (outsideEuropeClass === 'first-class' && outsideEuropeDistance === 'short') {
      trt = 1744.42;
    } else if (outsideEuropeClass === 'first-class' && outsideEuropeDistance === 'medium') {
      trt = 4980.96;
    } else if (outsideEuropeClass === 'first-class' && outsideEuropeDistance === 'long') {
      trt = 8310.39;
    }

    outsideEuropeTotal = (flyOutsideEurope * trt) / 12;

    total.trav2 += outsideEuropeTotal;

    console.log(`[trav2] total: ${total.trav2}`);

    console.table([['Fly within Europe', flyWithinEurope, withinEuropeClass, withinEuropeDistance, withinEuropeTotal], ['Fly oustide Europe', flyOutsideEurope, outsideEuropeClass, outsideEuropeDistance, outsideEuropeTotal]]);

    updateProgressBar(total.trav2);
  } else {
    const inputValue = getInputValueByName('[name="trav2"]')[0];

    if (inputValue) {
      fl_c = parseInt(inputValue.value);
    }

    updateProgressBar(fl_c);

    const accordion = document.querySelector("[data-slide='trav3'] [uk-accordion]");
    const accordionElements = accordion.querySelectorAll('li');
    const accordionElementsCount = accordionElements.length;

    if (fl_c > accordionElementsCount) {
      import(
        /* webpackChunkName: "createFlight" */
        /* webpackMode: "lazy" */
        './createFlight'
      ).then(({
        default: createFlight
      }) => {
        for (let i = accordionElementsCount; i < fl_c; i++) {
          createFlight(i, accordion)
        }
      }).catch(error => {});
    } else if (fl_c < accordionElementsCount) {
      for (let i = 0; i < (accordionElementsCount - fl_c); i++) {
        $(accordion.lastElementChild).find('.select2.from').select2('destroy');
        $(accordion.lastElementChild).find('.select2.to').select2('destroy');

        accordion.removeChild(accordion.lastElementChild);

        document.querySelector("[data-slide='trav3'] > .uk-flex > div:first-of-type").lastElementChild.remove();
      }
    }
  }
};

getSlideValues.trav3 = () => {
  total.trav3 = 0;

  const accordion = document.querySelector("[data-slide='trav3'] [uk-accordion]")

  const consoleTable = [];
  consoleTable.push(['flightId', 'direction', 'class_type', 'long1', 'lat1', 'long2', 'lat2', 'DLong', 'DLat', 'A', 'C', 'dist', 'total'])

  accordion.querySelectorAll('li').forEach((target, id) => {
    const flightId = target.getAttribute('data-flight-id');

    let direction = getInputValueByName(`[name='trav3-direction-${flightId}']`)[0].value;
    let class_type = getInputValueByName(`[name='trav3-class-${flightId}']`)[0].value;

    let long1;
    let lat1;
    let long2;
    let lat2;

    getInputValueByName('[name="trav3"]', target).forEach((item) => {
      if (item.type === 'airport-name-from') {
        const airport = airports.find(a => a.text === item.value);

        long1 = parseFloat(airport.longLat.split(",")[0]) * (Math.PI / 180);
        lat1 = parseFloat(airport.longLat.split(",")[1]) * (Math.PI / 180);
      } else if (item.type === 'airport-name-to') {
        const airport = airports.find(a => a.text === item.value);

        long2 = parseFloat(airport.longLat.split(",")[0]) * (Math.PI / 180);
        lat2 = parseFloat(airport.longLat.split(",")[1]) * (Math.PI / 180);
      }
    });

    let DLong = long2 - long1;
    let DLat = lat2 - lat1;
    let A;
    let C;
    let dist;

    A = Math.pow(Math.sin(DLat / 2.0), 2.0) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(DLong / 2.0), 2.0);
    C = 2.0 * Math.asin(Math.sqrt(A));

    dist = 6376.5 * C;

    let flightTotal = 0;

    if (direction === 'one-way' && class_type === 'first-class') {
      flightTotal = (dist * 0.55376) / 12;
    } else if (direction === 'one-way' && class_type === 'business') {
      flightTotal = (dist * 0.40149) / 12;
    } else if (direction === 'one-way' && class_type === 'economic') {
      flightTotal = (dist * 0.1384453) / 12;
    } else if (direction === 'two-way' && class_type === 'first-class') {
      flightTotal = (dist * 0.55376 * 2) / 12;
    } else if (direction === 'two-way' && class_type === 'business') {
      flightTotal = (dist * 0.40149 * 2) / 12;
    } else if (direction === 'two-way' && class_type === 'economic') {
      flightTotal = (dist * 0.1384453 * 2) / 12;
    }

    consoleTable.push([flightId, direction, class_type, long1, lat1, long2, lat2, DLong, DLat, A, C, dist, flightTotal]);

    if (!isNaN(flightTotal)) {
      total.trav3 += flightTotal;
    }
  });

  console.log(`[trav3] total: ${total.trav3}`);
  console.table(consoleTable)

  updateProgressBar(total.trav3);
};

let ac_t;

getSlideValues.trav4 = () => {
  ac_t = [];

  getInputValueByName('[name="trav4"]').forEach(item => {
    ac_t.push(item.value);
  });

  console.log(`[trav4] ac_t: ${ac_t.join(',')}`);

  toggleHiddenClass(document.querySelector('[data-slide="trav6"]'), ac_t.indexOf('hotel') > -1);

  toggleHiddenClass(document.querySelector('[data-slide="trav5"]'), ac_t.filter(type => /apartment|tent|camping/.test(type)).length > 0);

  document.querySelectorAll('[data-slide="trav5"] input[type="number"]').forEach(input => {
    if (ac_t.indexOf(input.getAttribute('data-type')) < 0) {
      input.parentNode.parentNode.classList.add('uk-hidden');
    } else {
      input.parentNode.parentNode.classList.remove('uk-hidden');
    }
  });

  updateProgressBar(ac_t.length);
};

let ng_n = {};

getSlideValues.trav5 = () => {
  total.trav5 = 0;

  getInputValueByName('[name="trav5"]').forEach(item => {
    ng_n[item.type] = parseInt(item.value);

    if (item.type === 'apartment') {
      total.trav5 += ng_n[item.type] * 5.9 / 12;
    } else if (item.type === 'tent') {
      total.trav5 += ng_n[item.type] * 3.3 / 12;
    } else if (item.type === 'camping') {
      total.trav5 += ng_n[item.type] * 7.3 / 12;
    }
  });

  console.log(`[trav5] ng_n: ${ng_n}, total: ${total.trav5}`);

  updateProgressBar(Object.keys(ng_n).length);
};

let rm_n = {};

getSlideValues.trav6 = () => {
  total.trav6 = 0;

  getInputValueByName('[name="trav6"]').forEach(item => {
    let hotelStar;

    if (item.type.indexOf('rooms')) {
      hotelStar = item.type.split('-')[1];
      ng_n[hotelStar] = parseInt(item.value);
    } else if (item.type.indexOf('nights')) {
      hotelStar = item.type.split('-')[1];
      rm_n[hotelStar] = parseInt(item.value);
    }
  });

  const hotelStars = {
    1: 6.38,
    2: 9.38,
    3: 13.52,
    4: 23.31,
    5: 55.86,
  }

  Object.keys(rm_n).forEach(star => {
    total.trav6 += ng_n[star] * rm_n[star] * hotelStars[star] / 12;
  });

  console.log(`[trav6] total: ${total.trav6}`);

  updateProgressBar(total.trav6);
};

getSlideValues.l1 = () => {
  const inputValue = getInputValueByName('[name="l1"]')[0];

  if (inputValue) {
    cur = inputValue.value.toLowerCase();
  }

  console.log(`[l1] cur: ${cur}`);

  updateProgressBar(cur);

  document.querySelectorAll('.uk-form-label.currency').forEach(label => label.innerHTML = {
    usd: '$',
    eur: '&euro;',
    pln: 'zł'
  } [cur] || '');
};

getSlideValues.l2 = () => {
  const inputValue = getInputValueByName('[name="l2"]')[0];

  if (inputValue) {
    dit_ty = inputValue.value;
  }

  console.log(`[l2] dit_ty: ${dit_ty}`);

  updateProgressBar(dit_ty);
};

getSlideValues.l3 = () => {
  const inputValue = getInputValueByName('[name="l3"]')[0];

  if (inputValue) {
    gros_cur = parseInt(inputValue.value);

    total.l3 = {
      eur: 0.847,
      usd: 0.619,
      pln: 0.223
    } [cur] * gros_cur * 4;
  }

  console.log(`[l3] gros_cur: ${gros_cur}, total: ${total.l3}`);

  updateProgressBar(gros_cur);
};

getSlideValues.l4 = () => {
  const inputValue = getInputValueByName('[name="l4"]')[0];

  if (inputValue) {
    tkot_cur = parseInt(inputValue.value);

    total.l4 = {
      eur: 0.377,
      usd: 0.276,
      pln: 0.099
    } [cur] * tkot_cur * 4;
  }

  console.log(`[l4] tkot_cur: ${tkot_cur}, total: ${total.l4}`);

  updateProgressBar(tkot_cur);
};

getSlideValues.l5 = () => {
  total.l5 = 0;

  const proteins = {
    lamb: 3.92,
    chicken: 0.69,
    tofu: 0.0835,
    tuna: 0.03,
    beef: 2.7,
    pork: 1.21,
    egg: 0.94,
    salmon: 0.22,
  };

  getInputValueByName('[name="l5"]').forEach(item => {
    total.l5 += parseInt(item.value) * proteins[item.type] * 4;
  });

  console.log(`[l5] total: ${total.l5}`);

  updateProgressBar(total.l5 > 0);
};

getSlideValues.l6 = () => {
  total.l6 = 0;

  const proteins = {
    lamb: 3.92,
    chicken: 0.69,
    tofu: 0.0835,
    tuna: 0.03,
    beef: 2.7,
    pork: 1.21,
    egg: 0.94,
    salmon: 0.22,
  };

  getInputValueByName('[name="l6"]').forEach(item => {
    total.l6 += parseInt(item.value) * proteins[item.type] * 4;
  });

  console.log(`[l6] total: ${total.l6}`);

  updateProgressBar(total.l6 > 0);
};

getSlideValues.l7 = () => {
  total.l7 = 0;

  const starch = {
    pasta: 0.181,
    rice: 0.27,
    bread: 0.0295,
    potatoes: 0.29,
  };

  getInputValueByName('[name="l7"]').forEach(item => {
    total.l7 += parseInt(item.value) * starch[item.type] * 4;
  });

  console.log(`[l7] total: ${total.l7}`);

  updateProgressBar(total.l7 > 0);
};

getSlideValues.l8 = () => {
  total.l8 = 0;

  const fruitsAndVegetables = {
    apples: 0.04,
    bananas: 0.048,
    peas: 0.09,
    carrots: 0.04,
    berries: 0.2,
  };

  getInputValueByName('[name="l8"]').forEach(item => {
    total.l8 += parseInt(item.value) * fruitsAndVegetables[item.type] * 4;
  });

  console.log(`[l8] total: ${total.l8}`);

  updateProgressBar(total.l8 > 0);
};

getSlideValues.l9 = () => {
  total.l9 = 0;

  const drinks = {
    coffee: 0.0554,
    'dairy-milk': 0.449,
    tea: 0.0554,
    wine: 1.208,
    cappuccino: 0.235,
    'coke-can': 0.17,
    water: 0.0432,
  };

  getInputValueByName('[name="l9"]').forEach(item => {
    total.l9 += parseInt(item.value) * drinks[item.type] * 4;
  });

  console.log(`[l9] total: ${total.l9}`);

  updateProgressBar(total.l9 > 0);
};

getSlideValues.l10 = () => {
  total.l10 = 0;

  const electronics = {
    'lcd-tv-small': 0.01997,
    'lcd-tv-medium': 0.02995,
    'lcd-tv-standard': 0.0579,
    'lcd-tv-large': 0.14725,
    'plasma-tv-small': 0.00997,
    'plasma-tv-medium': 0.01495,
    'plasma-tv-standard': 0.0289,
    'plasma-tv-large': 0.0735,
    'led-tv-small': 0.02538,
    'led-tv-medium': 0.03807,
    'led-tv-standard': 0.0736,
    'led-tv-large': 0.18717,
    'desktop-computer': 0.0362,
    'laptop-computer': 0.0148,
    speakers: 0.01,
    'gaming-system': 0.0448,
  };

  getInputValueByName('[name="l10"]').forEach(item => {
    let itemTotal = 0;

    if (item.type === 'lcd-tv' || item.type === 'plasma-tv' || item.type === 'led-tv') {
      const tvSize = document.querySelector(`[data-type="${item.type}-size"]`).value || 'standard';

      itemTotal = parseInt(item.value) * electronics[`${item.type}-${tvSize}`] * 4;
    } else {
      itemTotal = parseInt(item.value) * electronics[item.type] * 4;
    }

    if (!isNaN(itemTotal)) {
      total.l10 += itemTotal;
    }
  });

  console.log(`[l10] total: ${total.l10}`);

  updateProgressBar(total.l10 > 0);
};

getSlideValues.l11 = () => {
  total.l11 = 0;

  getInputValueByName('[name="l11"]').forEach(item => {
    if (parseInt(item.value) > 0) {
      total.l11 = parseInt(item.value) * (item.value.indexOf('GB') > -1 ? 1000 : 1) * 0.000682;
    }
  });

  console.log(`[l11] total: ${total.l11}`);

  updateProgressBar(total.l11 > 0);
};

let min_no = 0;

getSlideValues.l12 = () => {
  total.l12 = 0;

  getInputValueByName('[name="l12"]').forEach(item => {
    if (parseInt(item.value) > 0) {
      min_no = parseInt(item.value);
      total.l12 = min_no * 0.00115 * 4;
    }
  });

  console.log(`[l12] min_no: ${min_no}, total: ${total.l12}`);

  updateProgressBar(total.l12 > 0);
};

getSlideValues.l13 = () => {
  getInputValueByName('[name="l13"]').forEach(item => {
    plu_y = item.value;
  });

  console.log(`[l13] plu_y: ${plu_y}`);

  updateProgressBar(plu_y);
};

getSlideValues.l14 = () => {
  total.l14 = 0;

  const cottonClothes = {
    shirts: 9.687,
    't-shirts': 4.29,
    sweaters: 16.6,
    'suit-jackets': 8.3,
    jackets: 96.78,
    skirts: 11.07,
    pants: 17.99,
  };

  getInputValueByName('[name="l14"]').forEach(item => {
    total.l14 += (parseInt(item.value) * cottonClothes[item.type]) / 12;
  });

  console.log(`[l14] total: ${total.l14}`);

  updateProgressBar(total.l14 > 0);
};

getSlideValues.l15 = () => {
  total.l15 = 0;

  const polyesterClothes = {
    shirts: 7.465,
    't-shirts': 3.305,
    sweaters: 12.797,
    'suit-jackets': 6.398,
    jackets: 42.658,
    skirts: 8.531,
    pants: 13.863,
  };

  getInputValueByName('[name="l15"]').forEach(item => {
    total.l15 += (parseInt(item.value) * polyesterClothes[item.type]) / 12;
  });

  console.log(`[l15] total: ${total.l15}`);

  updateProgressBar(total.l15 > 0);
};

getSlideValues.l16 = () => {
  total.l16 = 0;

  const jackets = {
    leather: 63.064,
    wool: 185.136,
  };

  getInputValueByName('[name="l16"]').forEach(item => {
    total.l16 += (parseInt(item.value) * jackets[item.type]) / 12;
  });

  console.log(`[l16] total: ${total.l16}`);

  updateProgressBar(total.l16 > 0);
};

let myChart;

getSlideValues.l17 = () => {
  total.l17 = 0;

  const accessories = {
    'leather-shoes': 15.766,
    'polyester-shoes': 21.329,
    'rubber-shoes': 24.351,
    'canvas-shoes': 14.99,
    'leather-bags': 12.559,
    'polyester-bags': 16.99,
    'rubber-bags': 19.398,
    'canvas-bags': 11.941,
  };

  getInputValueByName('[name="l17"]').forEach(item => {
    total.l17 += (parseInt(item.value) * accessories[item.type]) / 12;
  });

  console.log(`[l17] total: ${total.l17}`);

  updateProgressBar(total.l17 > 0);

  let h_total = 0;
  let tran_total = 0;
  let trav_total = 0;
  let l_total = 0;

  Object.keys(total).forEach(key => {
    if (!isNaN(total[key])) {
      if (key.indexOf('h') === 0) {
        h_total += total[key];
      } else if (key.indexOf('tran') === 0) {
        tran_total += total[key];
      } else if (key.indexOf('trav') === 0) {
        trav_total += total[key];
      } else if (key.indexOf('l') === 0) {
        l_total += total[key];
      }
    }
  });

  const total_all = h_total + tran_total + trav_total + l_total;

  document.querySelector('.score .accordion h1').innerHTML = total_all.toFixed(2) + ' kg';

  if (myChart) {
    myChart.destroy();
  }

  const ctx = document.getElementById('chart');
  myChart = new Chart(ctx, {
    type: 'doughnut',
    data: {
      labels: [
        'Household',
        'Transportation',
        'Travel',
        'Lifestyle'
      ],
      datasets: [{
        data: [h_total, tran_total, trav_total, l_total],
        backgroundColor: [
          '#E3A6A8',
          '#4748F0',
          '#94CCE0',
          '#E5CC52'
        ],
        hoverBorderWidth: 0,
        hoverOffset: 2
      }],
    },
    options: {
      plugins: {
        legend: {
          display: false,
          // position: 'bottom',
        },
      },
    },
  });

  document.querySelectorAll('.score .accordion li').forEach((li, index) => {
    const bar = li.querySelector('.bar');

    const map = {
      0: h_total,
      1: tran_total,
      2: trav_total,
      3: l_total,
    };

    const percValue = (100 * map[index]) / total_all;

    bar.querySelector('div').style.width = percValue + '%';
    bar.querySelector('span').innerHTML = percValue.toFixed(0) + '%';
  });

  const sendResultsForm = document.querySelector('#send-results');

  const isENG = document.body.classList.contains('eng');

  sendResultsForm.querySelector('#user_email').setAttribute('data-pristine-required-message', isENG ? 'This field is required' : 'To pole jest wymagane');
  sendResultsForm.querySelector('#user_email').setAttribute('data-pristine-email-message', isENG ? 'Invalid email address' : 'Błędny adres e-mail');

  let defaultConfig = {
    // class of the parent element where the error/success class is added
    classTo: 'form-wrapper',
    errorClass: 'has-danger',
    successClass: 'has-success',
    // class of the parent element where error text element is appended
    errorTextParent: 'form-wrapper',
    // type of element to create for the error text
    errorTextTag: 'div',
    // class of the error text element
    errorTextClass: 'text-help'
  };

  // create the pristine instance
  const sendResultsFormValidation = new Pristine(sendResultsForm, defaultConfig);

  sendResultsForm.addEventListener('submit', e => {
    e.preventDefault();

    if (sendResultsFormValidation.validate()) {
      $.ajax({
        type: "POST",
        url: "sendResults.php",
        data: {
          lang: isENG ? 'eng' : 'pl',
          user_email: $("#user_email").val(),
          total_results: (total_all / 1000).toFixed(2),
          h_results: (h_total / 1000).toFixed(2),
          tran_results: (tran_total / 1000).toFixed(2),
          trav_results: (trav_total / 1000).toFixed(2),
          l_results: (l_total / 1000).toFixed(2),
        },
        dataType: "json",
        encode: true,
      }).done(function (data) {
        document.querySelector('.result-send.success').classList.remove('uk-hidden');
        document.querySelector('main').classList.add('uk-hidden');
      }).fail(data => {
        document.querySelector('.result-send.error').classList.remove('uk-hidden');
        document.querySelector('main').classList.add('uk-hidden');
      });
    }
  });

};

getSlideValues.l18 = () => {};
