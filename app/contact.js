import Pristine from "pristinejs";

const contactForm = document.querySelector('#contact-form');

const isENG = document.body.classList.contains('eng');

contactForm.querySelector('#user_email').setAttribute('data-pristine-required-message', isENG ? 'This field is required' : 'To pole jest wymagane');
contactForm.querySelector('#user_email').setAttribute('data-pristine-email-message', isENG ? 'Invalid email address' : 'Błędny adres e-mail');

contactForm.querySelector('#message_text').setAttribute('data-pristine-required-message', isENG ? 'This field is required' : 'To pole jest wymagane');

let defaultConfig = {
  // class of the parent element where the error/success class is added
  classTo: 'form-wrapper',
  errorClass: 'has-danger',
  successClass: 'has-success',
  // class of the parent element where error text element is appended
  errorTextParent: 'form-wrapper',
  // type of element to create for the error text
  errorTextTag: 'div',
  // class of the error text element
  errorTextClass: 'text-help'
};

// create the pristine instance
const contactFormValidation = new Pristine(contactForm, defaultConfig);

contactForm.addEventListener('submit', e => {
  e.preventDefault();

  if (contactFormValidation.validate()) {
    $.ajax({
      type: "POST",
      url: "sendMail.php",
      data: {
        user_email: $("#user_email").val(),
        message_text: $('#message_text').val(),
      },
      dataType: "json",
      encode: true,
    }).done(data => {
      document.querySelector('.uk-container.form').classList.add('uk-hidden');
      document.querySelector('.uk-container.done').classList.remove('uk-hidden');
    }).fail(data => {
      document.querySelector('.uk-container.form').classList.add('uk-hidden');
      document.querySelector('.uk-container.fail').classList.remove('uk-hidden');
    });
  } else {
    
  }
});
