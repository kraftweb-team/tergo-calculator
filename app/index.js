import UIkit from 'uikit';
import Icons from 'uikit/dist/js/uikit-icons';
import Headroom from "headroom.js";

UIkit.use(Icons);

const isMobile = window.isMobile && window.isMobile.any;
const isENG = document.body.classList.contains('eng');

if (!isMobile) {
  const headroom = new Headroom(document.querySelector('header'));
  // initialise
  headroom.init();
}

if (isMobile) {
  document.querySelector('#offcanvas-nav').firstElementChild.classList.add('uk-offcanvas-bar');

  UIkit.offcanvas('#offcanvas-nav', {
    mode: 'none',
    overlay: true,
  });
}

const createCookieBar = () => {
  const closeCookieBar = () => {
    window.localStorage.setItem('tergo', Date.now());

    cookieBar.remove();
  };

  const cookieBar = document.createElement('div');
  cookieBar.classList.add('cookieBar', 'uk-padding');

  const cookieBarContainer = document.createElement('div');
  cookieBarContainer.classList.add('uk-container', 'uk-flex', 'uk-flex-between', 'uk-flex-top');
  cookieBar.appendChild(cookieBarContainer);

  cookieBarContainer.innerHTML = `<p class="uk-width-1-1 uk-width-auto uk-margin-remove-bottom">${isENG ? 'This website uses cookies to improve your experience.' : 'Ta strona używa ciasteczek (cookies), dzięki którym nasz serwis może działać lepiej.'}<br/><a href="${isENG ? 'privacy-and-cookies-policy.html' : 'polityka-prywatnosci-i-plikow-cookies.html'}">${isENG ? 'Learn more' : 'Dowiedz się więcej'}<img src="img/link_arrow.svg" /></a></p>`; 

  const cookieButton = document.createElement('div');
  cookieButton.innerHTML = isENG ? 'Accept' : 'Akceptuję';
  cookieButton.classList.add('uk-button');

  cookieButton.addEventListener('click', closeCookieBar);

  cookieBarContainer.appendChild(cookieButton);

  const closeCookie = document.createElement('div');
  closeCookie.setAttribute('uk-icon', 'close');
  closeCookie.classList.add('closeCookie');

  closeCookie.addEventListener('click', closeCookieBar);

  cookieBarContainer.appendChild(closeCookie);

  document.body.appendChild(cookieBar);
}

if (!localStorage.getItem('tergo')) {
  createCookieBar();
}

document.querySelectorAll('.tooltip').forEach(tooltip => {
  const tooltipParent = tooltip.parentNode;
  const tooltipContent = tooltip.querySelector('.tooltip-content');

  tooltip.addEventListener('mouseenter', () => {
    tooltipContent.style.width = tooltipParent.offsetWidth + 'px';

    const tooltipLeftOffset = tooltipParent.getBoundingClientRect().left - tooltip.getBoundingClientRect().left

    tooltipContent.style.left = tooltipLeftOffset + 'px';
    tooltipContent.querySelector('img').style.left = tooltip.getBoundingClientRect().left - tooltipParent.getBoundingClientRect().left + (tooltip.offsetWidth / 2) + 'px';

    tooltipContent.style.display = 'block';
  });

  tooltip.addEventListener('mouseleave', () => {
    tooltipContent.style.display = '';
  });
});

