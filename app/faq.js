import holmes from "holmes.js";

const holmesInstance = holmes({
  input: '.uk-input',
  find: '.uk-accordion-content p',
  dynamic: true,
  onHidden: (el) => {
    const li = el.parentNode.parentNode;

    li.classList.add('hidden');

    const accordion = el.parentNode.parentNode.parentNode;
    const section = accordion.parentNode.parentNode;

    if (!accordion.querySelector('li:not(.hidden)')) {
      section.classList.add('hidden');
    }
  },
  onVisible: (el) => {
    const li = el.parentNode.parentNode;

    li.classList.remove('hidden');

    const accordion = el.parentNode.parentNode.parentNode;
    const section = accordion.parentNode.parentNode;

    section.classList.remove('hidden');
  },
});
holmesInstance.start();
