import airportsCountries from './airportsCountries';
import airports from './airports';

const isMobile = window.isMobile && window.isMobile.any;
const isENG = document.body.classList.contains('eng');

const createFlight = (id, parent) => {
  const li = document.createElement('li');
  li.setAttribute('data-flight-id', id);
  li.innerHTML = `<a class="uk-accordion-title" href="#"><span>${isENG ? 'Flight' : 'Lot'}</span><img src="img/remove-icon.png" class="remove"/></a>
  <div class="uk-accordion-content">
    <div class="uk-flex uk-form-controls">
    <label class="uk-form-label uk-margin-bottom uk-margin-right"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-direction-${id}" data-type="item" value="one-way" checked>${isENG ? 'one-way trip' : 'podróż w jedną stronę'}</label>
    <label class="uk-form-label uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-direction-${id}" data-type="item" value="two-way">${isENG ? 'two-way trip' : 'podróż w dwie strony'}</label>
    </div>
    <div class="uk-flex uk-form-controls">
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom uk-margin-remove-left@m"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="economic" data-label="${isENG ? 'economic' : 'klasa ekonomiczna'}" checked>${isENG ? 'economic' : 'klasa ekonomiczna'}</label>
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="business" data-label="${isENG ? 'business' : 'klasa biznes'}">${isENG ? 'business' : 'klasa biznes'}</label>
    <label class="uk-form-label input-row uk-width-1-3 uk-width-auto@m uk-margin-bottom"><input class="uk-radio circle uk-margin-small-right" type="radio" name="trav3-class-${id}" data-type="class" value="first-class" data-label="${isENG ? 'first class' : 'pierwsza klasa'}">${isENG ? 'first class' : 'pierwsza klasa'}</label>
    </div>
    <div class="uk-flex uk-form-controls uk-margin-top">
    <label style="width: 15%">${isENG ? 'From' : 'Z'}:</label>
    <div class="uk-flex uk-flex-column uk-margin-bottom" style="width: 85%">
      <select class="select2 airport-country-${id} from" style="width: 100%" type="select" data-type="airport-country" name="trav3">
      <option value="">${isENG ? 'Country' : 'Kraj'}</option>
      </select>
      <select class="select2 airport-name-${id} from" style="width: 100%" type="select" data-type="airport-name-from" name="trav3">
      <option value="">${isENG ? 'Airport' : 'Lotnisko'}</option>
      </select>
    </div>
    </div>
    <div class="uk-flex uk-form-controls">
    <label style="width: 15%">${isENG ? 'To' : 'Do'}:</label>
    <div class="uk-flex uk-flex-column" style="width: 85%">
      <select class="select2 airport-country-${id} to" style="width: 100%" type="select" data-type="airport-country" name="trav3">
      <option value="">${isENG ? 'Country' : 'Kraj'}</option>
      </select>
      <select class="select2 airport-name-${id} to" style="width: 100%" type="select" data-type="airport-name-to" name="trav3">
      <option value="">${isENG ? 'Airport' : 'Lotnisko'}</option>
      </select>
    </div>
    </div>
  </div>`;

  parent.appendChild(li);

  li.querySelector('.remove').addEventListener('click', e => {
    e.preventDefault();

    li.parentNode.removeChild(li);

    const planes = document.querySelector("[data-slide='trav3'] > .uk-flex > div:first-of-type");

    planes.removeChild(planes.lastElementChild);
  });

  let airportTo;
  let airportFrom;

  const countryFromSelect = $(`.airport-country-${id}.from`).select2({
    data: airportsCountries,
    placeholder: isENG ? 'Country' : 'Kraj',
  });
  const airportFromSelect = $(`.airport-name-${id}.from`).select2({
    data: [],
    placeholder: isENG ? 'Airport' : 'Lotnisko',
  });

  airportFromSelect.on('select2:select', e => {
    const data = e.params.data;

    airportFrom = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = airportFrom.substr(0, isMobile ? 12 : 15) + '...' + (airportTo ? ' &times; ' + airportTo.substr(0, isMobile ? 12 : 15) + '...' : '');
  });

  countryFromSelect.on('select2:select', e => {
    const data = e.params.data;

    const airportsForCountry = airports.filter(airports => airports.country === data.text);
    airportsForCountry.unshift({
      id: -1,
      text: isENG ? 'Airport' : 'Lotnisko',
    });

    airportFromSelect.select2('destroy');
    airportFromSelect.find('option').remove().end().select2({
      placeholder: isENG ? 'Airport' : 'Lotnisko',
      data: airportsForCountry,
    });
  });

  const countryToSelect = $(`.airport-country-${id}.to`).select2({
    data: airportsCountries,
    placeholder: isENG ? 'Country' : 'Kraj',
  });
  const airportToSelect = $(`.airport-name-${id}.to`).select2({
    data: [],
    placeholder: isENG ? 'Airport' : 'Lotnisko',
  });

  airportToSelect.on('select2:select', e => {
    const data = e.params.data;

    airportTo = data.text;

    li.querySelector('.uk-accordion-title span').innerHTML = (airportFrom ? airportFrom.substr(0, isMobile ? 12 : 15) + '...' : '') + ' &times; ' + airportTo.substr(0, isMobile ? 12 : 15) + '...';
  });

  countryToSelect.on('select2:select', e => {
    const data = e.params.data;

    const airportsToCountry = airports.filter(airports => airports.country === data.text);
    airportsToCountry.unshift({
      id: -1,
      text: isENG ? 'Airport' : 'Lotnisko',
    });

    airportToSelect.select2('destroy');
    airportToSelect.find('option').remove().end().select2({
      placeholder: isENG ? 'Airport' : 'Lotnisko',
      data: airportsToCountry,
    });
  });

  if (id < 11) {
    const planeImage = document.createElement('img');
    planeImage.src = './img/plane_small.png';
    planeImage.style.top = (id % 2 ? ((id - 1) * 90 + 60) : (id * 90)) + 'px'

    document.querySelector("[data-slide='trav3'] > .uk-flex > div:first-of-type").appendChild(planeImage);
  }
};

export default createFlight;