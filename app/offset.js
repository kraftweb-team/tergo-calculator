const isENG = document.body.classList.contains('eng');

$(document).ready(function () {
  const countryArray = [];

  import(
    /* webpackChunkName: "elec" */
    /* webpackMode: "lazy" */
    './elec'
  ).then(({
    default: elec
  }) => {
    elec1 = elec;

    Object.keys(elec).forEach((name, index) => {
      countryArray.push({
        id: index,
        text: name
      });
    });

    $('.select2.country').select2({
      data: countryArray,
      placeholder: isENG ? 'Country' : 'Kraj',
    });
  }).catch(error => {});

  const submitButton = document.querySelector('.select-country-form .uk-button');
  const countrySelect = document.querySelector('.select2.country');

  let selectedCountry;

  submitButton.addEventListener('click', e => {
    e.preventDefault();

    if (countrySelect.selectedIndex > 0) {
      selectedCountry = countrySelect.selectedOptions[0].innerText;

      document.querySelectorAll('.uk-section').forEach((section, index) => {
        if (index === 0) {
          section.classList.add('uk-hidden');
        } else {
          section.classList.remove('uk-hidden');
        }
      });
    }
  });

  const submitPrevent = e => {
    e.preventDefault();
  }

  document.querySelectorAll('.offset-project').forEach(project => {
    if (!project.classList.contains('active')) {
      project.querySelector('a.uk-button').addEventListener('click', submitPrevent);
    }

    project.querySelector('.offset-project__title').addEventListener('click', () => {
      const otherProject = project.parentNode.parentNode.querySelector('.offset-project.active');

      otherProject.classList.remove('active');

      const otherProjectSumbitButton = otherProject.querySelector('a.uk-button');

      otherProjectSumbitButton.classList.add('blocked');
      otherProjectSumbitButton.addEventListener('click', submitPrevent);

      project.classList.add('active');
      project.querySelector('a.uk-button').classList.remove('blocked');
      project.querySelector('a.uk-button').removeEventListener('click', submitPrevent);
    });

    project.querySelectorAll('.offset-project__payment .uk-button.white').forEach(button => {
      button.addEventListener('click', () => {
        button.parentNode.querySelector('.uk-active').classList.remove('uk-active');

        button.classList.add('uk-active');
      })
    });
  });
});
