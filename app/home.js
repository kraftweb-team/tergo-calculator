import Pristine from "pristinejs";

let windowMouseUpHandler;
let wallBouncingInterval;

const dragButton = document.querySelector('.drag-button');
const dragButtonWidth = dragButton.offsetWidth;

let dragButtonOffsetX = 0;

const wall = document.querySelector('.wall-wrapper');

const mouseMoveHandler = e => {
  wall.style.width = ((e.clientX - (dragButtonOffsetX - (dragButtonWidth / 2)) - wall.offsetLeft) / window.innerWidth) * 100 + '%';
};

windowMouseUpHandler = () => {
  window.removeEventListener('mousemove', mouseMoveHandler);
  window.removeEventListener('mouseup', windowMouseUpHandler);
};

dragButton.addEventListener('mousedown', (e) => {
  dragButtonOffsetX = e.offsetX;

  clearInterval(wallBouncingInterval);

  wall.classList.remove('transition');

  window.addEventListener('mousemove', mouseMoveHandler);
  window.addEventListener('mouseup', windowMouseUpHandler);
});

window.addEventListener('DOMContentLoaded', () => {
  wall.classList.add('transition', 'reveal');

  setTimeout(() => {
    wallBouncingInterval = setInterval(() => {
      wall.classList.add('bouncing');
  
      setTimeout(() => {
        wall.classList.remove('bouncing');
      }, 3100);
    }, 10000);
  }, 4000);
});

const newsletterForm = document.querySelector('#newsletter');

const isENG = document.body.classList.contains('eng');

newsletterForm.querySelector('#user_email').setAttribute('data-pristine-required-message', isENG ? 'This field is required' : 'To pole jest wymagane');
newsletterForm.querySelector('#user_email').setAttribute('data-pristine-email-message', isENG ? 'Invalid email address' : 'Błędny adres e-mail');

let defaultConfig = {
  // class of the parent element where the error/success class is added
  classTo: 'form-wrapper',
  errorClass: 'has-danger',
  successClass: 'has-success',
  // class of the parent element where error text element is appended
  errorTextParent: 'form-wrapper',
  // type of element to create for the error text
  errorTextTag: 'div',
  // class of the error text element
  errorTextClass: 'text-help'
};

const insertAfter = (newNode, existingNode) => {
  existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
};

// create the pristine instance
const newsletterFormValidation = new Pristine(newsletterForm, defaultConfig);

let newsletterFormSendInfo;

newsletterForm.querySelector('#user_email').addEventListener('keypress', () => {
  if (newsletterFormSendInfo) {
    newsletterFormSendInfo.remove();
  }
});

newsletterForm.addEventListener('submit', e => {
  e.preventDefault();

  if (newsletterFormValidation.validate()) {
    $.ajax({
      type: "POST",
      url: "userSave.php",
      data: {
        user_email: $("#user_email").val(),
      },
      dataType: "json",
      encode: true,
    }).done(data => {
      if (!newsletterFormSendInfo) {
        newsletterFormSendInfo = document.createElement('div');
        newsletterFormSendInfo.classList.add('uk-margin-small-top');
        insertAfter(newsletterFormSendInfo, document.querySelector('.form-wrapper'));
      }

      let message;

      if (data) {
        message = isENG ? 'Thank you for registering @TerGo. You will receive a confirmation email shortly.' : 'Dziękujemy za rejestrację. Wkrótce otrzymasz potwierdzenie na swój adres e-mail.';
      } else {
        message = isENG ? 'Your Email is already in database' : 'Twój adres e-mail jest już w bazie';
      }
      
      newsletterFormSendInfo.innerHTML = message;
    });
  }
});

