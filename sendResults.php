<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

if (isset($_POST['user_email'])) {
  $mail = new PHPMailer;

  $mail->isSMTP();                                      // Set mailer to use SMTP
  $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
  $mail->SMTPAuth = true;                               // Enable SMTP authentication
  $mail->Username = 'hello@tergo.io';                 // SMTP username
  $mail->Password = 'Terracredits';                           // SMTP password
  $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
  $mail->Port = 587;                                    // TCP port to connect to
  $mail->CharSet = "UTF-8";

  $mail->SetFrom('hello@tergo.io');
  $mail->addAddress($_POST['user_email']);     // Add a recipient
  $mail->addBCC('hello@tergo.io', "Mails @TerGo");

  if ($_POST['lang'] === 'eng') {
    $subject = 'Your carbon footprint: check your score on TerGo calculator! 🌍';
  } else {
    $subject = 'Twój ślad węglowy: sprawdź wynik obliczeń na kalkulatorze TerGo! 🌍';
  }

  $mail->Subject = $subject;

  if ($_POST['lang'] === 'eng') {
    $body = '<p>Hi!</p>
    <p>So, you care about our planet and you want to know your carbon footprint? TerGo\'s Carbon Footprint Calculator is a great choice! Here is your result:</p>
    <p><strong>Your annual carbon footprint is ' . $_POST['total_results'] . ' tons of CO<sub>2</sub></strong></p>
    <p style="padding-left: 30px"><strong>Details:</strong></p>
    <p style="padding-left: 30px"><strong>Household: ' . $_POST['h_results'] . ' tons of CO<sub>2</sub></strong><br/>
    <strong>Transportation: ' . $_POST['tran_results'] . ' kg CO<sub>2</sub></strong><br/>
    <strong>Travel: ' . $_POST['trav_results'] . ' tons of CO<sub>2</sub></strong><br/>
    <strong>Lifestyle: ' . $_POST['l_results'] . ' tons of CO<sub>2</sub></strong></p>
    <p>Congratulations!</p>
    <p>Now you know your CO<sub>2</sub> emissions. This is the first step towards making more conscious choices every day! You don\'t need to change who you are - just change some daily actions small decisions that will have a big impact on supporting climate action and environmental protection. Each day you can decide what to do to help if you want to do a little more or a little less for our planet. It\'s all up to you! and we know you can do it!</p>
    <p>Do you want to improve your score and lower your carbon footprint? Read <a href="http://tergo.kraftweb.pl/blog-en.html">TerGo\'s blog</a>! Here you will find practical tips, useful information, and inspiration for making more eco-minded choices!</p>';
  } else {
    $body = '<p>Cześć!</p>
    <p>Troszczysz się o planetę i chcesz wiedzieć, jaki zostawiasz za sobą ślad? Kalkulator śladu węglowego TerGo to świetny wybór. Oto Twój wynik:</p>
    <p><strong>Twój roczny ślad węglowy wynosi ' . $_POST['total_results'] . ' ton CO<sub>2</sub></strong></p>
    <p style="padding-left: 30px"><strong>Szczegóły:</strong></p>
    <p style="padding-left: 30px"><strong>Gospodarstwo domowe: ' . $_POST['h_results'] . ' ton CO<sub>2</sub></strong><br/>
    <strong>Transport: ' . $_POST['tran_results'] . ' ton CO<sub>2</sub></strong><br/>
    <strong>Podróże: ' . $_POST['trav_results'] . ' ton CO<sub>2</sub></strong><br/>
    <strong>Styl życia: ' . $_POST['l_results'] . ' ton CO<sub>2</sub></strong></p>
    <p>Brawo!</p>
    <p>Znasz już poziom własnych emisji CO<sub>2</sub>. To pierwszy krok do podejmowania bardziej świadomych wyborów na co dzień! Bądź sobą, ale wybieraj trochę inaczej – zrobisz, co chcesz oraz mocniej wesprzesz ochronę klimatu i środowiska. Każdego dnia decyduj o tym, czy chcesz zrobić dla planety jeszcze więcej.</p>
    <p>Chcesz poprawić swój wynik i łatwo obniżyć swój ślad węglowy?</p>
    <p>Czytaj <a href="http://tergo.kraftweb.pl/blog.html">bloga TerGo</a>! Znajdziesz tam wiele praktycznych wskazówek i informacji, zainspirujesz się do ekodziałania!</p>';
  }

  $mail->Body = $body;
  $mail->IsHTML(true);


  if(!$mail->send()) {
    echo 'Message could not be sent.';
    //echo 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
    echo '1';
  }
}