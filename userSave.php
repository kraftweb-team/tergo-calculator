<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require 'vendor/autoload.php';

if (isset($_POST['user_email'])) {
  $userEmail = $_POST['user_email'];

  $servername = "mysql46.mydevil.net";
  $database = "m1268_temp_terra";
  $username = "m1268_temp_terra";
  $password = "YQ3RHeQ9RxRDnLnqHa8y";

  // Create connection
  $conn = mysqli_connect($servername, $username, $password, $database);

  // Check connection
  if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
  }

  $query = "SELECT terra_user_email FROM terra_pre_registrated_users WHERE terra_user_email = '" . $userEmail . "'";

  $result = mysqli_query($conn, $query);

  $check = mysqli_num_rows($result);

  if ($check > 0) {
    echo '0';
  } else {
    $query = "INSERT INTO terra_pre_registrated_users(
      terra_user_email, terra_user_first_name, terra_user_last_name, terra_user_ip_address, terra_user_registration_country
    ) VALUES (
      '" . $userEmail ."', '', '', '', ''    
    )";
  
    $result = mysqli_query($conn, $query);

    if ($result) {
      $mail = new PHPMailer;

      $mail->isSMTP();                                      // Set mailer to use SMTP
      $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
      $mail->SMTPAuth = true;                               // Enable SMTP authentication
      $mail->Username = 'hello@tergo.io';                 // SMTP username
      $mail->Password = 'Terracredits';                           // SMTP password
      $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;                            // Enable TLS encryption, `ssl` also accepted
      $mail->Port = 587;                                    // TCP port to connect to
      $mail->CharSet = "UTF-8";

      //Recipients
      $mail->setFrom('hello@tergo.io', 'Hello @TerGo');
      $mail->addAddress($userEmail);     // Add a recipient
      // $mail->addAddress('admin@terracredits.io', "Terra Admin");               // Name is optional
      $mail->addReplyTo('hello@tergo.io', 'Hello @TerGo');
      // $mail->addCC('cc@example.com');
      $mail->addBCC('hello@tergo.io', "Mails @TerGo");

      $mail->Subject = "Psst! You're on the list";
      $mail->Body = "
      <div style=\"width: 600px; margin: 30px auto 0; padding: 20px 0;\">
          
          <p style=\"margin-bottom: 30px; padding: 0 20px;\"><strong>Thank you for joining the TerGo community.</strong></p>
          
          <p style=\"padding: 0 20px;\">At TerGo we are creating an awesome platform to share ideas, give the mobile app tools, bring together and of course financially reward our members to take positive actions to help the environment.</p>  
          
          <p style=\"margin-bottom: 30px; padding: 0 20px;\">
              We will update you once our Beta version is ready for its exciting launch. 
              Have questions about TerGo? We'd love to hear from you. Just hit reply:) And remember with TerGo, it pays to go Green.   
          </p>
          
          <p style=\"margin-bottom: 30px; padding: 0 20px;\">Thank you and stay tuned.</p>
          
          <p style=\"padding: 0 20px;\"><strong>Thuy and the TerGo Team</strong></p>
          
      </div>
      
      <div style=\"width: 600px; margin: 0 auto 30px; padding: 0;\">
          <img src=\"http://tergo.io/images/footer.png\" alt=\"TerGo Carbon Trade\" style=\"width: 100%;\"/>
      </div>
      ";

      $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

      $mail->IsHTML(true);

      try {
        $mail->send();

        echo '1';
      } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
      }
    }
  }

  mysqli_close($conn);
}
?>