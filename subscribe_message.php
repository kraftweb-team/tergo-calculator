<?php

require_once "subscribe_email.php";


$cssClass = $subscribeError ? "alert-danger" : "alert-success";
$messageContent = $subscribeError ? $subscribeError["message"] : "
    Thank you for registering @TerGo. You will receive a confirmation email shortly. 
";

?>


<article class="col-12 col-lg-5">

    <div class="subscribe-message row text-center alert <?php echo $cssClass ?>">

        <span class="col-12"><?php echo $messageContent ?></span>

    </div>

</article>


<!--<script>-->
<!--$('html, body').animate({-->
<!--    scrollTop: $(".subscribe-message").offset().top-->
<!--}, 1500);-->
<!--</script>-->
